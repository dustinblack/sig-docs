# The `automotive-image-builder` tool

The `automotive-image-builder` tool uses OSBuild to build images based on objects called *manifests*, which are YAML files
that you use to define your OS image.

## Running `automotive-image-builder` from a container

The Automotive SIG maintains a container image that you can use to run `automotive-image-builder` in the
[automotive-osbuild Quay.io repo](https://quay.io/repository/centos-sig-automotive/automotive-osbuild?tab=tags).

To quickly run `automotive-image-builder` from a container, run the following shell script within your local
 [clone](getting_started.md#cloning-autosd-sample-images-and-automotive-image-builder)
of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images):

```console
$ sudo sample-images/auto-image-builder.sh build --target qemu --mode image \
--distro cs9 --arch x86_64 --export qcow2 images/minimal.mpp.yml cs9-qemu-minimal-image.x86_64.qcow2
```

If you already installed `automotive-image-builder` locally with its dependencies and you want to run it in a container,
use the following command with the `--container` option:

```console
$ ./automotive-image-builder --container build --mode package --target qemu \
--export qcow2 <path-to-my-manifest>.mpp.yml <my-image>.qcow2
```

!!! IMPORTANT
    If you build multiple images, you must run `make clean` and `dnf clean all` between builds to clear build artifacts and reset the DNF cache.

For more information about the container, see the
[automotive-osbuild](https://gitlab.com/CentOS/automotive/container-images/automotive-osbuild) repository.

## `automotive-image-builder` options

When you build an image with `automotive-image-builder`, you must specify some options in the build command:

```console
$ ./automotive-image-builder build --mode <package-or-image> --target <target> \
--export <export-format> <path-to-manifest>.mpp.yml <my-image>.qcow2
```

### Architecture

`--arch`
: The hardware architecture to build for (x86_64 or aarch64). If unspecified, the native
architecture is used.

!!! Note
    You can compose an image for any architecture, but you can only build one for the native architecture.

### Distributions

`--distro`
: Define the package repositories for the distribution you intend to use. The default is "cs9". View available distributions with the
  `automotive-image-builder list-dist` command. Available distributions include:

- autosd or autosd9
- cs9
- eln
- f40

  To extend the `list-dist` with custom distributions, add an `ipp.yml` in a directory called `/some/dir/distro` and pass
  `--include /some/dir` to the argument list.

### Modes

`--mode`
: Set the value to `package` or `image`. The default value is `image`.

- Use `package` to build a package-based OS image, which is useful for development and testing.
- Use `image` to build an OSTree image for use in production.

### Targets

`--target`
: The physical or virtual deployment target for your image. The default value is `qemu`.
  View the available targets with the `./automotive-image-builder list-targets` command. Available targets include:

- QEMU with aboot or grub
- KVM
- TI AM62, AM69, BeaglePlay, J784S4 EVM, and TDA4
- AWS
- Qualcomm QDrive3 and RideSX4
- ccimx93
- Windows PC
- Renesas R-Car
- Raspberry Pi 4
- NXP S32G2-VNP-RDB3

### Export formats

`--export`
: The image file type that you want to build. View the available export formats with the `./automotive-image-builder list-exports` command.
  Export formats available as of October 2024 include:

- **image:** A raw disk image with partitions
- **qcow2:** A qcow2 format disk image with partitions
- **ext4:** An ext4 filesystem containing just the rootfs partition (i.e., no boot partitions)
- **aboot:** An android boot system partition image and a boot partition
- **container:** A container image you can run with podman or docker
- **tar:** A tar file containing the basic rootfs files
- **ostree-commit:** An ostree repo with the commit built from the image
- **ostree-oci-image:** An oci image wrapping the ostree commit from ostree-commit
- **rpmlist:** A json file listing all the RPMs used in the image

## Next steps

For more information about using the `automotive-image-builder` tool with build options and manifests
to provision your hardware, see the flashing guide specific to your target hardware:

- [NXP](provisioning/flashing-nxp.md)
- [Qualcomm](provisioning/flashing-images-ride4.md)
- [Renasas](provisioning/flashing-renesas.md)
- [Texas Instruments](provisioning/flashing-texas-instruments.md)
