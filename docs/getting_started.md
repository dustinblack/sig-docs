# Getting started

Similar to the upstream-downstream relationship between CentOS Stream and Red Hat Enterprise Linux (RHEL), AutoSD is the public, upstream,
in-development repository for Red Hat In-Vehicle Operating System (OS). AutoSD is a binary Linux distribution based on CentOS Stream but with a
some divergences. For example, AutoSD relies on the `kernel-automotive` package rather than the CentOS Stream kernel package.
Like CentOS Stream, AutoSD is open to public contributions and enables anyone to preview Red Hat In-Vehicle OS, which makes it a convenient
solution for development. Community members and current and potential Red Hat customers and partners can explore AutoSD
to see what might land in Red Hat In-Vehicle OS.

!!! IMPORTANT
    AutoSD is fully compatible with Red Hat In-Vehicle OS, but it is not certified for functional safety (FuSa) or commercially supported.

![Automotive Stream Distribution vs CentOS Stream](img/AutoSD_CS.jpg)

The Automotive SIG manages several artifacts:

* **Automotive Stream Distribution (AutoSD)**: This project is a binary distribution developed within the SIG that is a public,
in-development preview of the upcoming Red Hat In-Vehicle Operating System (OS). <!-- For more information, see [about/about-autosd-benefits.md].-->
* **RPM repositories**: These are RPM repositories produced by the Automotive SIG to enhance AutoSD. New packages or features can be
developed and hosted there to expand the capabilities of AutoSD.
<!-- For more information, see [Automotive SIG repositories](contributing/sig-repos.md).-->
* **Sample images**: These are images built with [OSBuild](https://www.osbuild.org/) using example packages from AutoSD, Automotive SIG
repositories, or other sources. For more information, see
[Working with sample images, sample manifests, and `automotive-image-builder` targets](about-manifests.md) and
 [Deploying sample apps](deploying-sample-apps-containers.md).

The Automotive SIG builds and tests are multiple AutoSD images daily.

## Cloning AutoSD sample images and `automotive-image-builder`

Before you can experiment with AutoSD on your host, you must clone sample images for AutoSD and the `automotive-image-builder` tool.

**Prerequisites**

* git

**Procedure**

1. Clone the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images):

    ```console
    git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
    ```

    !!! IMPORTANT
        When cloning the repo the first time, you must pass the `--recursive` option to also
        clone the submodules.

        If you have an existing `sample-images` git repository that doesn't have the submodule, you can clone it after the fact using
        the following command:

        ```console
        git submodule update --init
        ```

        If you update to the latest version of a branch using `git pull`, use the `--recurse-submodules` option to also
        update the submodule, or run `git submodule update` after the pull.

**Next steps**

Set up your containerized or VM developer environment on your [Linux](getting-started-on-linux.md) or
[MacOS](getting-started-on-macos.md) <!--, or [Windows](getting-started-on-windows.md)-->host.
