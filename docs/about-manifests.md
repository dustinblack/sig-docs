# Sample OSBuild manifests

[OSBuild](https://www.osbuild.org/) *manifests* are YAML files where you define and customize your OS image with the following specifications:

- Included repositories and RPMs
- Enabled services
- Configurations of the hostname configuration or time zone
- Stages that modify the file system of the image

The `images/` directory in your
[local clone of the `sample-images/` repository](getting_started.md#cloning-autosd-sample-images-and-automotive-image-builder)
contains OSBuild manifest files, indicated by the `mpp.yml` extension.
The `mpp.yml` files contain the instructions, or OSBuild stages, to create new OS images.

## Available sample manifests

The Automotive SIG provides the following sample manifests for you to use as you experiment with AutoSD:

1. `container.mpp.yml`

    Use this sample container manifest to experiment with the configurations on how to to install and run a containerized application.

1. `containerperf.mpp.yml`

    Use this sample containerperf manifest to experiment with performance test configurations for containers, such as load and stress tests.

1. `developer.mpp.yml`

    Use this sample developer manifest as an example of how to configure your development environment. It contains metadata,tools and dependencies
    useful for doing development work with AutoSD.

1. `encrypted.mpp.yml`

    Use this sample encryption manifest to experiment on how to set up an encnryted disk using Linux Unified Key Setups (LUKS)
    with `automotive-image-builder`.

1. `gadget.mpp.yml`

    Use this sample gadget manifest to turn Raspberry Pi 4 into a USB gadget with a non-OSTree AutoSD image that you can use, without putting
    the PI on your corporate network.

1. `minimal.mpp.yml`

    Use this sample minimal manifest as an example of how to configure a foundation for development environments. It contains the minimum and most
    essential settings required for an image to function properly without additional complexity.

1. `ocibased.mpp.yml`

    Use this [Open Container Initiative (OCI)](https://opencontainers.org/)-based sample manifest to experiment on how to use
    `automotive-image-builder` with a `bootc`.

1. `qmcontainer.mpp.yml`

    Use this QM container manifest to experiment on how to use ASIL B and QM mixed criticality with containers in the QM partition.

1. `rollbackalttarget.mpp.yml`

    Use this OSTree-based manifest to experiment with the
    [`ostree-rollback-to-rescue` RPM package](https://copr.fedorainfracloud.org/coprs/g/centos-automotive-sig/next/package/ostree-rollback-to-rescue/).

1. `upgrade-demo.mpp.yml`

    Use this upgrade-demo sample manifest to experiment with [unattended update](https://sigs.centos.org/automotive/building/unattended_updates/)
    and automatic fallback features from OSTree, the external watchdog, and greenboot health checks.

For more information about working with manifest files, see
[Embedding RPM packages in the AutoSD image](building/packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image).
