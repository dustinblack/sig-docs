# Getting started on Windows

Set up your development environment with the prerequisite tools and repositories you need to quickly start building
AutoSD images in either a preconfigured container or a virtual machine (VM).

**Prerequisites**

* A machine running 64-bit Windows 8.1 or later
* [`pacman` package manager from MSYM2](https://www.msys2.org/docs/windows_support/)
* The local [clone you made](getting_started.md#cloning-autosd-sample-images-and-automotive-image-builder)
  of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images) with the
  `automotive-image-builder` submodule

## Quick start: Building AutoSD images using a preconfigured development container

You can use the preconfigured AutoSD development container as a lightweight base development environment if you want to
build an AutoSD image without launching and configuring a development VM.
This container includes `systemd`, the AutoSD container orchestration tool `bluechi`, `automotive-image-builder`, and other development tools.

**Procedure**

1. Install [Podman 5](https://packages.msys2.org/base/mingw-w64-podman):

    ```console
    pacman -S mingw-w64-podman
    ```

1. From the `sample-images` directory, launch the `automotive-osbuild` container by specifying `$PWD:/host` to mount the
`sample-images` directory in the `/host` directory inside the container:

    ```console
    cd sample-images
    sudo podman run -it -v /dev:/dev -v /root:/root -v $PWD:/host --rm --privileged --pull=newer \
    --security-opt label=type:unconfined_t quay.io/centos-sig-automotive/automotive-osbuild
    ```

1. Copy the `sample-images` repository on the host to the container:

    ```console
    # cp -r /host /root
    ```

1. Run `automotive-image-builder` from a container using either the shell script or your local application.

      1. To quickly run `automotive-image-builder` from a container without installing or configuring any other dependencies,
         run the following shell script from the `sample-images/` directory within your local
        [clone](getting_started.md#cloning-autosd-sample-images-and-automotive-image-builder)
        of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images):

          ```console
          $ sudo auto-image-builder.sh build --target qemu --mode image \
          --distro autosd9 --arch x86_64 --export <file_type> images/<manifest-name>.mpp.yml <my-image>.<file_type>
          ```

      1. To have the build pull a
         [containerized version of OSBuild](https://quay.io/repository/centos-sig-automotive/automotive-osbuild?tab=tags), run your locally
         installed and configured `automotive-image-builder` with the `--container` option:

            ```console
            cd /root/host/automotive-image-builder
            ./automotive-image-builder --container build --mode image --target qemu \
            --export <file_type> ../images/<manifest-name>.mpp.yml <my-image>.<file_type>
            ```

1. Change to the `/root/host/automotive-image-builder` directory:

    ```console
    cd /root/host/automotive-image-builder
    ```

1. Move your image file from `/root/host/automotive-image-builder` to the `/host` directory, so that you can access
   it in the `sample-images` directory outside of the container:

    ```console
    mv <my-image>.<file_type> /host
    ```

1. Exit the container:

    ```console
    exit
    ```

## Quick start: Booting prebuilt AutoSD images in a QEMU VM

A virtualized AutoSD development environment is similar to a AutoSD production environment. The virtualized platform is useful for building and
testing applications intended to run on a AutoSD system or to build new AutoSD images that you can flash onto automotive hardware or use on other
systems.

Download and uncompress a prebuilt AutoSD operating system (OS) image, and then launch a VM from the image. This VM is your
development environment, where you can customize and build your own AutoSD images.

All prebuilt AutoSD images can be found in the [nightly repo](https://autosd.sig.centos.org/AutoSD-9/nightly/).
For more information, see [Sample OS images](sample-non-sample-images.md). To explore the sample images, see
[sample images](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/).

In this example, use the `developer` image for either x86 or aarch64. The `developer` image has extra storage compared to other images.

**Procedure**

1. Install [QEMU](https://www.qemu.org/download/#windows)
   and [XZ Utils](https://packages.msys2.org/base/mingw-w64-xz):

    ```console
    pacman -S mingw-w64-ucrt-x86_64-qemu
    pacman -S ming-w64-x86_64-xz
    ```

1. Identify the name of the latest nightly image for your host architecture, and store the value in a variable called `AUTOSD_IMAGE_NAME`.

    !!! note

        The Automotive SIG uploads images every day using unique build IDs, which causes the name of the image to change frequently.
        For more information about available nightly images, their purposes, and their naming conventions,
        see [Sample OS images](sample-non-sample-images.md).

      1. If your host machine has an `x86_64` CPU, retrieve the name of the latest `x86_64` image:

    ```console
        $ export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/ | \
        grep -oE 'auto-osbuild-qemu-autosd9-developer-regular-x86_64-([0-9]+)\.([A-Za-z0-9]+)\.qcow2\.xz' | \
    head -n 1)"
    ```

      1. If your host machine has an `aarch64` SoC, retrieve the name of the latest `aarch64` image:

        ```console
        $ export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/ | \
        grep -oE 'auto-osbuild-qemu-autosd9-developer-regular-aarch64-([0-9]+)\.([A-Za-z0-9]+)\.qcow2\.xz' | \
        head -n 1)"
        ```

3. Download the image:

    ```console
    curl -o autosd9-developer-regular-x86_64.qcow2.xz https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/$AUTOSD_IMAGE_NAME
    ```

4. Uncompress the compressed `.xz` image file:

    ```console
    xz -d autosd9-developer-regular-x86_64.qcow2.xz
    ```

1. To launch a VM from the image, run the
  [`automotive-image-runner`](https://gitlab.com/CentOS/automotive/src/automotive-image-builder/-/blob/main/automotive-image-runner?ref_type=heads)
  script from the the `automotive-image-builder` directory of your local clone of the
[AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images):

    ```console
    cd sample-images/automotive-image-builder
    ./automotive-image-runner <path>/autosd9-developer-regular-x86_64.qcow2
    ```

1. Log in as the `root` user with the default password, `password`.

     !!! NOTE
        To enable ssh access, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`. Then you can access the machine
        with `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

## Quick start: Building customized AutoSD images in a QEMU VM

Repeat the procedure in
[Quick start: Booting prebuilt AutoSD images in a QEMU VM](#quick-start-booting-prebuilt-autosd-images-in-a-qemu-vm),
to download and run the latest nightly developer image, which has extra storage compared to the other sample images. Then, expand
the disk size, so you can use the `automotive-image-builder` tool to create customized system images using your custom manifest `.mpp.yml` file.

For more information about the preconfigured manifest files the Automotive SIG provides as starter examples you can modify, see the
[Sample OSBuild manifests](about-manifests.md#sample-osbuild-manifests).

For more in-depth information about how to package your applications and embed them in a customized manifest you can then use to generate
your customized OS image, see
[Packaging applications with RPM](building/packaging_apps_with_rpm.md) and
[Embedding RPM packages in the AutoSD image](building/packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image) sections.

**Prerequisites**

* The latest [nightly](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/)
  `autosd9-dev-reg-x86_64.qcow2` image

**Procedure**

1. Extend the virtual disk of your `.qcow2` development image, so that you have enough space to build
your custom AutoSD images and facilitate your development work.

      1. On the host, resize your development image. In this example, set the disk size to `30G`, which is 30GiB:

         ```console
         qemu-img resize autosd9-dev-reg-x86_64.qcow2 30G
         ```

      1. Launch your virtual AutoSD development environment:

         ```console
         cd sample-images/automotive-image-builder
         ./automotive-image-runner autosd9-dev-reg-x86_64.qcow2
         ```

      1. Log in with the `guest` user and the default password `password`. The `root` user password is also `password`.

1. Install the `parted` partition management tool:

    ```console
    dnf -y install parted
    ```

      1. Run `parted` to extend the size of `/dev/vda`

          ```console
          parted /dev/vda
          ```

      1. Resize the `/dev/vda3` partition to fill the space available to that partition:

          ```console
          resizepart 3 100%
          ```

      1. Enter `quit` to exit the `parted` tool.

      1. Enlarge the file system:

         ```console
         resize2fs /dev/vda3
         ```

1. In your development VM, create a custom OSBuild manifest file you can configure according to your requirements:

      1. Change to the `automotive-image-builder` directory:

          ```console
          cd sample-images/automotive-image-builder
          touch <my-manifest>.mpp.yml
          ```

      1. Build the OS image from your custom `my-manifest.mpp.yml` in your preferred file format:

          ```console
          $ ./automotive-image-builder build --mode package --target qemu \
          --export qcow2 <my-manifest>.mpp.yml <my-image>.<file_type>
          ```

        !!! NOTE
            For more information about the export file types `automotive-image-builder` supports, see [Export formats](about-automotive-image-builder.md#export-formats) in the
            [`automotive-image-builder` options](about-automotive-image-builder.md#automotive-image-builder-options) section.

            For more information about image naming conventions, see [AutoSD sample images](sample-non-sample-images.md#autosd-sample-images).

        !!! NOTE
            If you build multiple images, you must run `make clean` and `dnf clean all` between builds to clear build artifacts and reset the DNF cache.

1. Export the image file to the host.

2. On the host, launch a VM from your new image:

    ```console
    ./automotive-image-runner <my-image>.<file_type>
    ```

For more information about how to build your own customized AutoSD images, see [Building and running your customized OS image](building/building_an_os_image.md#building-and-running-your-customized-os-image).
