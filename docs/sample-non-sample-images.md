# Sample OS images

In an effort to continuously deliver working OS images, the Automotive SIG builds
several OS images for AArch64 or x86_64 systems or VMs, which you can find in the
[AutoSD nightly folder](https://autosd.sig.centos.org/AutoSD-9/nightly/).
These images are updated as the CentOS Stream or AutoSD RPM repositories change.

For more information about the OSBuild manifests used to build these sample OS images, see
[About `automotive-image-builder` and OSBuild manifests](about-manifests.md).

!!! NOTE
    Regular images are based on RPM packages rather than OSTree file system trees.

!!! IMPORTANT
    Do not use sample or non-sample images in production.

## AutoSD sample images

The [`AutoSD-9/nightly/sample-images` folder](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/) contains images built only with
AutoSD RPMs. Most images are available for both aarch64 and x86_64, except those built for Raspberry Pi, which are
raw RPM-based images only for aarch64.

The naming convention for these image files is: `$distro-$target-$image_type-$os_itype.$arch.$file_type`.
For example, the file name `autosd-qemu-developer-regular.x86_64.qcow2` represents the following values:

- `distro`: autosd

- `target`: qemu

- `image_type`: developer

- `os_type`: regular

- `arch`: x86_64

- `file_type`: qcow2

**Minimal OSTree**
: These OSTree-based QEMU images are examples of a minimal image and have neither a package manager nor SSH:

- `auto-osbuild-qemu-autosd9-minimal-ostree-aarch64`
- `auto-osbuild-qemu-autosd9-minimal-ostree-x86_64`

**Developer OSTree and regular**
: These OSTree- or RPM-based QEMU images include standard developer tools for building and working with RPMs, images, OSTree binaries,
  and containers:

- `auto-osbuild-qemu-autosd9-developer-ostree-aarch64`
- `auto-osbuild-qemu-autosd9-developer-ostree-x86_64`
- `auto-osbuild-qemu-autosd9-developer-regular-aarch64`
- `auto-osbuild-qemu-autosd9-developer-regular-x86_64`

**QA OSTree**
: These OSTree-based QEMU images built for quality assurance testing are a minimal image with
  extra packages such as: SSH, beaker, rsync, sudo, wget, time, nfs-utils, git, jq:

- `auto-osbuild-qemu-autosd9-qa-ostree-aarch64`
- `auto-osbuild-qemu-autosd9-qa-ostree-x86_64`

**QM OSTree**
: This OSTree-based QEMU image is an example of mixed criticality within AutoSD, orchestrated by BlueChi, between quality-managed (QM) and
  Automotive Safety Integrity Level (ASIL) partitions, where applications, containers, processes, and independent instances of Podman and systemd
  run while maintaining freedom from interferance (FFI):

- `auto-osbuild-qemu-autosd9-qm-ostree-x86_64`

**Raspberry Pi minimal, developer, QA OSTree, and QA regular**
: These are minimal, developer, and QA RPM- or OSTree-based aarch64 images for Raspberry Pi 4:

- `auto-osbuild-rpi4-autosd9-developer-regular-aarch64`
- `auto-osbuild-rpi4-autosd9-minimal-regular-aarch64`
- `auto-osbuild-rpi4-autosd9-qa-ostree-aarch64`
- `auto-osbuild-rpi4-autosd9-qa-regular-aarch64`

## AutoSD plus non-sample images

The [`AutoSD-9/nightly/non-sample-images` folder](https://autosd.sig.centos.org/AutoSD-9/nightly/non-sample-images/) contains x86_64 and
aarch64 images built from AutoSD RPMs and customized with packages from other RPM repositories, such as CentOS Stream 9
and [COPR](https://copr.fedorainfracloud.org/):

**Container OSTree**
: These are OSTree-based QEMU images built to demonstrate how to include containers in images:

- `auto-osbuild-qemu-autosd9-container-ostree-aarch64`
- `auto-osbuild-qemu-autosd9-container-ostree-x86_64`

**Raspberry Pi Gadget**
: This is an RPM-based Raspberry Pi 4 image built to be used as a [USB gadget](building/gadget.md):

- `auto-osbuild-rpi4-autosd9-gadget-regular-aarch64`
