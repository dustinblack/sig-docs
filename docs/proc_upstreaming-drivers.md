# Upstreaming hardware drivers

As a hardware supplier, upstreaming your hardware drivers is crucial for seamless integration into AutoSD and the open source
Red Hat In-Vehicle OS ecosystem.

To upstream your driver, you must submit your driver module into the same broad, open source upstream community review and acceptance process that
Red Hat developers follow. When Red Hat developers upstream code, it flows downstream through Fedora, CentOS Stream, and into AutoSD.
Red Hat developers take automotive-specific code and configurations that have also been accepted upstream and into AutoSD, and merge it with a
reconfiguration of RHEL to create Red Hat In-Vehicle OS.

## Advantages of upstreaming your drivers

By upstreaming your hardware drivers and embracing an open source approach, you can reap numerous tangible advantages while
contributing to a vibrant ecosystem, leveraging the collective knowledge and expertise of our community while also fueling
your innovation and continuous improvement.
Engaging with open source technologies not only encourages collaboration and knowledge sharing but also enhances the quality
and reliability of the solutions we create together.

Some of the benefits of upstreaming your drivers as you experiment with AutoSD
include the following:

**Increased visibility and market adoption**
: Mainline inclusion boosts mindshare and hardware compatibility,
leading to greater market adoption and brand recognition.

**Improved compatibility**
: Mainline inclusion ensures broader Linux distribution support and provides hardware compatibility out-of-the-box.
Whereas out-of-tree drivers continuously need to maintain compatibility with the baseline.

**Reduced maintenance burden**
: Linux community support means a shared responsibility for driver maintenance, which allows silicon vendors like you to maintain fewer versions
of the OS while benefiting from broader testing, so you can focus resources on other critical areas.

**Collaboration and feedback**
: Community collaboration provides valuable feedback, leading to higher quality drivers, improved performance, and enhanced reliability.

## Upstreaming a hardware driver

To integrate your hardware driver into AutoSD, submit your hardware driver as a
[patch](https://www.kernel.org/doc/html/latest/process/submitting-patches.html#submittingpatches) to the Linux mainline kernel.

**Prerequisites**

Determine whether or not your organization plans to upstream this hardware driver.

* If you plan to upstream your driver, proceed with this procedure.
* If you’re not sure, see [Advantages of upstreaming your drivers](#advantages-of-upstreaming-your-drivers) and
  [Ideal hardware](hardware_enablement_program.md#ideal-hardware).
* If you do not plan to upstream your driver and plan to ship your driver independently instead, see
  [Out-of-tree drivers](hardware_enablement_program.md#out-of-tree-driver-modules).

**Procedure**

1. Clone the upstream kernel master branch.

    ```git
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    ```

    !!! TIP
        Start with the `arm64 defconfig` kernel configuration.

    !!! IMPORTANT
        Do not make any code changes to this kernel.

1. Add any calibration options specific to your board or driver.

    !!! Note
        If you compile your drivers with the kernel source, then you might mask changes you make to the core parts of the automotive kernel.

1. Confirm that your driver is compatible with the AutoSD automotive kernel by verifying that
your driver functions without needing to change the kernel source code. If it doesn't and you must change
the kernel source code for your driver to function as designed, you can either:

      * Modify the architecture of your driver module to avoid the need to change the kernel.
      * Propose changes to the upstream kernel community to accommodate the specific needs of your driver module, which requires that you also
       contribute the driver code to substantiate the kernel changes.

    !!! IMPORTANT
        If you are working on kernel drivers intended for use in Red Hat In-Vehicle OS, contact
        Red Hat for driver requirements.

**Additional resources**

* [Submitting patches: the essential guide to getting your code into the kernel](https://www.kernel.org/doc/html/latest/process/submitting-patches.html#submittingpatches)
* [Linux kernel patch submission checklist](https://www.kernel.org/doc/html/latest/process/submit-checklist.html)
* [Getting the Code Right in _A Guide to the Development Process_](https://www.kernel.org/doc/html/latest/process/4.Coding.html#getting-the-code-right)
* [Development tools for the kernel](https://www.kernel.org/doc/html/latest/dev-tools/index.html)
* [Kernel Testing Guide](https://www.kernel.org/doc/html/latest/dev-tools/testing-overview.html#)
* [Static analysis tools](https://www.kernel.org/doc/html/latest/dev-tools/testing-overview.html#static-analysis-tools)
* [Checkpatch](https://www.kernel.org/doc/html/latest/dev-tools/checkpatch.html#)
* [A guide to the Kernel Development Process](https://docs.kernel.org/process/development-process.html)
* [Fedora/ARK Kernel documentation](https://cki-project.gitlab.io/kernel-ark/)
