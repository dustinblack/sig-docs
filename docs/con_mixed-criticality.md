# Mixed criticality

Mixed criticality is the capacity of a vehicle to run different workloads that demand different levels of compliance with functional safety. A
mixed-criticality system is a system containing hardware or software that can execute different applications of different criticality, such as
safety-critical and non-safety critical. A mixed-criticality system must determine which applications are prioritized when they share resources.
The Red Hat In-Vehicle OS intends to certify up to the ASIL-B level of safety. At this level, mixed criticality is essential to the safe operation
of the vehicle. You must isolate and constrain your workloads so that you can adequately configure resource allocation and priority.

## Conventional mixed criticality in automotive

QM applications have more relaxed requirements than ASIL applications because they are not critical to the safety of the people in and around the
vehicle. Isolating these types of applications is crucial to safety.

Historically, the automotive industry used discrete physical devices to run ASIL applications and QM applications, achieving freedom from
interference (FFI) through compartmentalization of the physical infrastructure. QM applications could not interfere with ASIL applications, and ASIL
applications could not interfere with other ASIL applications. The industry then used VMs to run QM applications, compartmentalizing ASIL and QM
applications to guarantee FFI with fewer physical devices in the vehicles, reducing cost, and simplifying maintenance and architecture.

![Three ECUs, one for ASIL B, QM, and ASIL D, each with its own hardware, OS, and application](img/automotive-architecture-past-distributed-ecus.png "Yesterday’s automotive architectures: Distributed ECUs")

![Shared hardware with a hypervisor that each host a VM, one for ASIL B, QM, and ASIL D, each with its own OS and application](img/automotive-architecture-present-domain-vms.png "Today’s automotive architectures: Domain VMs")

AutoSD can continue to run existing VMs dedicated to QM applications if you prefer to continue to use your current application isolation mechanism.
However, because AutoSD is a Linux-based OS, you also have support today for tomorrow’s isolation mechanisms: _containers_.

![Shared hardware with a hypervisor that hosts an ASIL B VM with a container for each application and container management by AutoSD and a separate ASIL D OS with another application](img/automotive-architecture-future-zonal-containers.png "Tomorrow’s automotive architectures: Zonal containers")

## Containers in automotive

Containers are a process isolation mechanism relying on features of the Linux kernel and a container image to fully isolate a process or application
from the host running them. Containers and container images are reliable, scalable and trusted mechanisms to build, deploy and isolate processes
from each other and from the host while ensuring full consistency as they move between environments, such as:

* Dev, test, and production
* Public, private, and hybrid cloud or onsite data centers

Containers are reliable isolation mechanisms, not only in software development but also in shared environments where security is paramount. Public
services, such as public OpenShift offerings or shared OpenShift instances, show the trust placed in containers to protect against resource abuse
and malicious attacks, ensuring uninterrupted service for all users. These isolation measures are essential for protecting against various threats,
including attempts by malicious actors to monopolize resources or DDoS attack other users. This highlights the versatility of containers beyond
development, showcasing their effectiveness in maintaining operational integrity and security in various use cases. Containers have the following
advantages in the automotive context:

* Configure mixed criticality at a process level by running your entire software stack on a single OS in a way that isolates ASIL and QM
applications from each other and from the rest of the system
* Secure operational integrity by protecting against resource abuse and malicious attacks, ensuring uninterrupted service for all users
* Save cost and simplify maintenance by reducing the number of physical devices in the vehicle
* Save resources by running more workloads on the same hardware or by reducing the hardware capability for your existing workloads
* Run different applications that have different runtime environments without conflicts and without requiring several VMs
* Decouple the application lifecycle from the OS lifecycle
* Use standardized OCI container technologies and benefit from the entire ecosystem dedicated to containerized applications

In a containerized architecture, you run the entire software stack on a single OS that isolates ASIL and QM applications from each other and the
rest of the system, then you configure criticality at the process level.

![Comparison diagram of a three generations of vehicle hardware E/E architecture evolution: distributed architecture with 14 functional ECUs to represent a feature-in-a-box configuration, like classic Autosar; a domain architecture with three domain controllers and 10 ECUs to represent a domain-features-in-a-box configuration, like Adaptive Autosar; and a zonal architecture with one high-performance compute platform and as many zonal ECU or SoC nodes as automotive engineers decide they need (7 shown) to represent several domains in the same box](img/automotive-architecture-vs-distributed-domain-zonal.png "Comparison of distributed, domain, and zonal automotive architectures")

### Container-orchestrated mixed criticality

Safety is the top priority for AutoSD and Red Hat In-Vehicle OS. Safety workloads with an ASIL rating are more important than non-safety workloads,
and you must isolate and constrain your workloads so that you adequately configure resource allocation and priority to architect
freedom from interference (FFI). Balancing these workloads requires careful orchestration of the following aspects of the system:

**Resource management**

* Computing, graphics, audio, input
* Shared devices, such as requests, limits, priorities, ownership

**Security**

* Isolation
* Minimized attack surface

**Lifecycle management**

* Start, stop, resume, reboot, update, manage the software stack

**Heterogeneous environments**

* Android and AutoSD workloads

During the research and development phase of container-based mixed criticality, Red Hat considered that third-party companies might develop QM
applications. QM applications must never interfere with ASIL applications and so the configuration of ASIL-rated containerized applications and
those nonessential applications containerized within the QM partition is critical. If configured incorrectly, two containers might interfere with
each other, so it is important to understand how different configurations can react and interact. These interactions can be complex and when
overlooked can lead to unforeseen behavior. To prevent accidental misconfiguration for QM applications, AutoSD contains a dedicated preconfigured
mechanism to ensure that non-ASIL applications cannot interfere with ASIL-rated applications. This mechanism is called the QM partition.

In this framework, system architects can carefully assign the resources, networking interfaces, or user IDs that are available for all of the QM
applications. The QM partition includes configuration to enable the termination of QM applications if the system comes under memory pressure. As a
last resort, the kernel can stop the entire QM partition to free memory for ASIL applications.

### The preconfigured QM partition

To guarantee FFI between safety-critical ASIL applications and nonessential QM applications, AutoSD constrains QM workloads within the preconfigured
QM partition.

The QM partition is a subsection of the host filesystem with its own `root` that it has application containers nested within it. The default
subsection of the host filesystem used by the QM partition is `/usr/lib/qm`.

![Filesystem diagram that shows an ASIL rootfs with an instance of systemd and /usr, /bin, /etc, /var, /lib, and /home directories and an podman instance within /var/lib/containers. The rootfs for the preconfigured QM partition is stored in /usr/lib/qm. The QM partition has its own instances of systemd, podman, cgroups, SELinux, namespaces, and SECcomp](img/container-isolation-detail.png "The preconfigured QM partition")

The QM partition hosts dedicated instances of `systemd` and Podman that are unique and distinct from the ASIL `systemd` and Podman instances. These
dedicated instances guarantee that the start, stop, or reload of a QM application cannot affect a similar action taken for the ASIL applications.

![The container isolation architecture diagram shows how to run your entire software stack on a single OS in a way that isolates ASIL and QM containers, applications, and guest OS instances from one another and the rest of the system](img/container-isolation-simple.jpg "AutoSD container isolation")

**ASIL B container**
: A containerized ASIL B application.

**ASIL B app**
: An ASIL B application installed directly on the OS image.

**QM partition**
: A preconfigured partition where you install all QM workloads.

**QM container**
: A containerized QM application installed in the QM partition.

**QM app**
: A QM application installed in the QM partition.

**QM Guest OS**
: A virtual guest OS launched inside the QM partition.

### SELinux in the QM partition

To properly isolate QM applications from each other, the QM partition supports not only running nested containers, but also has a special SELinux
configuration.

In a standard Linux environment, each process and each file in the system has an SELinux label. This label enables SELinux to restrict, at the
kernel level, the actions that each process can take, according to a set of policies that define the rules for each label.

In a standard container configuration, there is just one SELinux label that encompasses all processes in a container: `container_t`. This label,
combined with the unique ID of each container, enables SELinux to constrain the container process. However, this is a limited level of constraint
because SELinux does not differentiate between processes running inside the container.

In the AutoSD QM partition, SELinux has preconfigurations and an additional rule set for QM-specific SELinux labels. There is an SELinux label for
processes in the QM partition and additional labels for each container within the QM partition. This means that SELinux differentiates between and
isolates containerized processes within the QM partition, and therefore, isolates and constrains your nested containerized software. You run nested
containers inside the QM partition that has all of the security mechanics that apply to regular containers.

With this mechanism, the ASIL processes can see, watch, and stop the QM processes running in the QM partition. Conversely, QM processes running in
the QM partition cannot see or interfere with any process outside of the QM partition.

The QM partition runs SELinux and isolated instances of `systemd`, Podman, and any other applications. This isolates all applications from each
other and the rest of the system. Therefore, ASIL and QM applications run within a single system, which saves resources, grants flexibility, and
enables per-application lifecycle management decoupled from the OS lifecycle.

To preserve FFI by using the QM partition in your mixed-criticality OS, see
[Deploying applications in the QM partition](building/deploying_apps_in_qm_partition.md).

To use containers in your mixed-criticality automotive OS, see [Containerizing applications](building/containerizing_apps.md).

**Additional resources**

* [Understanding containers](https://www.redhat.com/en/topics/containers)
* [How Linux PID namespaces work with containers](https://www.redhat.com/sysadmin/linux-pid-namespaces)
