# About deprecated content

Deprecated content contains topics about tools and methods that are no longer preferred, or for which newer tools and methods exist.
