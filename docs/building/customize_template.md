# Customizing the OSbuild templates

There are three ways one can customize an OSbuild template:

- Manually edit the template
- Overriding variables on the CLI
- Overriding variables via a custom distro file

We will not be covering here the first method as this is the least sustainable,
changes made to the template would need to be rebased every time a change is
made to the repository.

First, let's discuss about these variables.

## Template variables

The manifests currently include a number of variables, which appear in different `mpp-vars`
sections of the OSbuild template manifests.

For example, a `mpp-vars` section may look like the following example:

```json
  "mpp-vars": {
    "rootfs_uuid": {"mpp-format-string": "{__import__('uuid').uuid1()}"},
    "bootfs_uuid": {"mpp-format-string": "{__import__('uuid').uuid1()}"},
    "rootfs_size": 4294967296,
    "homefs_size": {"mpp-format-string": "{rootfs_size}"}
  },
```

This example defines four variables: `rootfs_uuid`, `bootfs_uuid`, `rootfs_size`
and `homefs_size`.

- `rootfs_uuid` and `bootfs_uuid` dynamically generate when you run the equivalent of the following
  python code:

```python
import uuid
uuid.uuid1()
```

- `rootfs_size` is hardcoded to `4294967296` bytes.

- `homefs_size` is equal to `rootfs_size`.

## Overriding variables via a custom distro file

The simplest way to override variables using a customized file in the [`distro/`](
https://gitlab.com/CentOS/automotive/sample-images/-/tree/main/distro)
folder in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

As an example, we will create a customized image pulling two RPMs from the [EPEL](
https://docs.fedoraproject.org/en-US/epel/) repository.

### Create the custom file

```console
cp distro/cs9.ipp.yml distro/custom.ipp.yml
```

### Edit the custom file

Open `distro/custom.ipp.yml` in your favorite text editor and adjust it so that
the diff between `cs9.ipp.yml` and `custom.ipp.yml` is as follow:

```diff
--- distro/cs9.ipp.yml 2022-04-12 10:20:18.130917511 +0200
+++ distro/custom.ipp.yml 2022-04-22 16:18:11.203590871 +0200
@@ -10,6 +10,8 @@
     baseurl: $distro_baseurl/AppStream/$arch/os/
   - id: automotive
     baseurl: https://buildlogs.centos.org/9-stream/automotive/$arch/packages-main/
+  - id: epel-9
+    baseurl: https://dl.fedoraproject.org/pub/epel/9/Everything/$arch/
   distro_devel_repos:
   - id: crb
     baseurl: $distro_baseurl/CRB/$arch/os/
@@ -21,3 +23,7 @@
   - id: crb-debug
     baseurl: $distro_baseurl/CRB/$arch/debug/tree/
   distro_module_id: platform:el9
+  extra_rpms:
+  - gnuplot
+  - python3-zmq
+  root_password: $6$3c5hyALn6Ge/XXwL$Qr961XQbJeCC/aRLNd4CagSRUa/x7tOoG2MrnKpPCrZxoNYSZ4N3bfFQO99A6vQUuAWOOoUclqxPa2DJ0Ylg90
```

As you can see, we've added a new repo named `epel-9` with its `baseurl`. Lower
down, we have defined a `extra_rpms` variable corresponding to a list with two packages:
`gnuplot` and `python3-zmq`. We have also changed the root password from `password` to
`password2` (the `guest` password remaining the same).

!!! note
    To encrypt the password you can use: `mkpasswd --method=SHA-512 --stdin`

### Create the image

Now that we have this `distro/custom.ipp.yml` created, if we ran `make help`
we will this in its output:

```console
...
custom-qemu-container-regular.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
custom-qemu-container-ostree.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
custom-qemu-container-direct.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
custom-qemu-developer-regular.x86_64.[img,qcow2,oci.tar,repo,rootfs,ext4,tar]
...
```

So we can create the image of our choice:

```console
make custom-qemu-developer-regular.x86_64.qcow2
```

### Boot and check

Once the image has been built, we can easily run it using `runvm`:

<!-- > Should this be changed to use aib runner? <-->

```console
./runvm custom-qemu-developer-direct.x86_64.qcow2
```

We can then log into the image with the `root` user, using `password2` as password
and check that the two new packages were installed:

```console
[root@localhost ~]# rpm -q python3-zmq gnuplot
python3-zmq-22.3.0-2.el9.x86_64
gnuplot-5.4.3-2.el9.x86_64
```
