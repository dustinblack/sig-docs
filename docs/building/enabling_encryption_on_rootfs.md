# Enabling encryption on the root filesystem

You can enhance the privacy of your AutoSD image by enabling LUKS2 encryption on the root filesystem. When you enable encryption, the root
filesystem becomes a dm-crypt partition using LVM2, and the necessary dependencies are added to the system to allow it to be unlocked.

The passphrase used for the initial encryption comes from the `luks_passphrase` variable, which defaults to `password`. However,
after the systems is booted, you can add other passphrases and remove the default password.

**Procedure**

- Enable encryption by adding `use_luks: true` to the `mpp-vars` section of your manifest file:

    ```console
    mpp-vars:
      name: <my-manifest>
      use_luks: true
    ```

## Enabling automatic unlocking with an initial passphrase

By default, the passphrase must be entered interactively from the initrd during boot, but this is not always possible. The AutoSD image supports
automatic unlocking. Enable this by setting the variable `luks_auto_unlock` to true.

If enabled, the initial passphrase, which is the value of the `luks_passphrase` variable, is copied into the root filesystem as
`/usr/.auto-unlock-key` and then propagated into the initrd. During boot, this key is automatically used to unlock the root partition.

**Procedure**

- Enable automatic unlocking by adding `luks_auto_unlock: true` to the `mpp-vars` section of your manifest file:

    ```console
    mpp-vars:
      name: <my-manifest>
      use_luks: true
      luks_auto_unlock: true
    ```

    !!! important
        On its own, this configuration is not secure because the password is visible in plain text in the initrd, and the initrd is not encrypted.
        This configuration is an initial step to encrypt a filesystem that can boot non-interactively. After you enable automatic unlocking with an
        initial passphrase, you can add a new, secure passphrase.

## Configuring TPM-based unlocking

When you build an AutoSD image, you initially encrypt it with a disposable passphrase that unlocks the device on first boot.
Trusted Platform Module (TPM) is a security feature in modern CPUs that allows you to establish a hardware-specific passphrase that unlocks the
device. After you configure TPM-based unlocking, the TPM discards the initial, disposable passphrase and uses the new hardware-specific passphrase
to automatically unlock the filesystem for each subsequent boot.

**Prerequisites**

- A system with the packages `osbuild-luks2` and `osbuild-lvm2` installed to support OSBuild org.osbuild.luks2 and org.osbuild.lvm2.

**Procedure**

- Enable TPM-based unlocking by adding `clevis-dracut` to the `extra_boot_rpms` section of your manifest file:

    ```console
    mpp-vars:
      name: <my-manifest>
      use_luks: true
      luks_auto_unlock: true
      extra_boot_rpms:
        - clevis-dracut
    ```

    !!! note
        Enabling TPM is supported with QEMU. When you build your AutoSD image, you must use the `--tpm2` switch.

    For more information, see
[Configuring manual enrollment of LUKS-encrypted volumes](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/security_hardening/configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption_security-hardening#configuring-manual-enrollment-of-volumes-using-clevis_configuring-automated-unlocking-of-encrypted-volumes-using-policy-based-decryption)
in the RHEL 9 _Security hardening_ guide.

**Next steps**

- Now that you have encrypted your root filesystem, you can build your AutoSD image. For more information, see
[Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).
- Alternatively, you can continue customizing your image. For more information, see
[Containerizing applications](containerizing_apps.md#containerizing-applications).

**Additional resources**

- [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)
