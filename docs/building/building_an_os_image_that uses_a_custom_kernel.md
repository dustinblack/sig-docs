# Building an AutoSD image that includes a custom kernel

On a native AArch64 architecture system, you can compile a custom version of the OS kernel and then build a version of the AutoSD operating system (OS)
image that includes the customized kernel.
You can then install and run the customized AutoSD OS in either a virtualized environment or on an ARM board.

!!! note
    **Cross-compilation is unsupported for kernel customizations**.

    You must compile custom kernels on AArch64 hardware that is running a supported Linux host.
    See [Building images](index.md) for more information about software and hardware requirements for AArch systems.

The overall process consists of the following general steps:

1. [Customize the kernel source, compile a new kernel, and build RPM files](#compile-a-customized-autosd-kernel-and-build-new-rpm-files).

1. [Build a custom AutoSD OS image and run it in a virtual environment](#build-an-autosd-image-with-a-custom-kernel-and-run-it-in-a-virtual-environment).

1. [Build a custom AutoSD OS image and run it on physical hardware](#build-an-autosd-image-with-a-custom-kernel-and-run-it-on-physical-hardware).

## Compile a customized AutoSD kernel and build new RPM files

On a [supported Linux host system](index.md) running on AArch64 hardware, compile a customized kernel and modules from the
CentOS Stream 9 kernel source code.

!!! note

    This procedure assumes that you are using a CentOS Stream 9 host running on ARM hardware. If you use a different host system, some steps in this procedure might not work.

**Prerequisites**

- You are running CentOS Stream 9 on AArch64 hardware. See [Building images](index.md) for more information about software and
     hardware requirements.

**Procedure**

1. Install the tools and dependencies necessary to compile the kernel:

     ```console
     sudo dnf update
     sudo dnf groupinstall "Development Tools"
     sudo dnf install ncurses-devel bison flex elfutils-libelf-devel openssl-devel dwarves
     ```

1. Clone the CentOS Stream automotive kernel source code Git repository to a local directory of your choice:

     ```console
     git clone --depth 1 --branch main-automotive https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git/
     ```

1. Go to the `centos-stream-9` directory that contains the source code:

     ```console
     cd centos-stream-9
     ```

1. Add to or modify the kernel source code according to your requirements.

    !!! Important
        You must stage and commit your changes in Git, or the `make` command will not pick up the changes and build them into the RPM packages.

1. Generate the RPM workspace, which includes the kernel spec file, for the build:

     ```console
     make -j$(nproc) dist-srpm
     ```

1. Install the dependencies necessary to build the kernel RPM files:

     ```console
     sudo dnf config-manager --set-enabled crb && sudo dnf builddep redhat/rpm/SPECS/kernel.spec
     ```

1. Compile the custom kernel and build the RPM files. The following example uses `_mykernel` as the custom string
     to be appended to the new versions of the RPM file names:

     ```console
     make -j$(nproc) DISTLOCALVERSION=_mykernel dist-all-rpms
     ```

     By appending a custom string, you can differentiate your custom build from the standard version.

    !!! Important
         The custom version string must consist of alphanumeric characters plus any of the following optional separators: `.`, `_`, `+`, `~`, and `^`.
         The string cannot contain spaces or certain other characters, such as hyphens (`-`). If the string contains illegal characters,
         the build will fail.

         See the ["Spec file format" RPM documentation](https://rpm-software-management.github.io/rpm/manual/spec.html)
         for more information about using the `~` and `^` separators in version strings.

After a successful build, RPM files are installed to the following directory: `./redhat/rpm/RPMS/aarch64/`.

## Build an AutoSD image with a custom kernel and run it in a virtual environment

After you create your custom kernel RPM files, you can build a custom AutoSD image that includes the files and will run in a QEMU
virtual machine.

**Prerequisites**

- You have successfully built RPM package files that contain kernel customizations.

- You have installed [QEMU](https://www.qemu.org/download/#linux).

**Procedure**

1. Go to your home directory:

     ```console
     cd ~
     ```

1. If you have not done so already, install the OSBuild utility:

     ```console
     sudo dnf install osbuild
     ```

1. Recursively clone the [sample-images](https://gitlab.com/CentOS/automotive/sample-images) Gitlab repository:

     ```console
     git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
     ```

1. Go to the `automotive-image-builder` directory:

     ```console
     cd sample-images/automotive-image-builder
     ```

1. Deploy the custom kernel RPM files:

    1. If you have not done so already, install the createrepo utility:

          ```console
          sudo dnf install createrepo
          ```

    1. Create a new directory to use as a local RPM repository:

          ```console
          sudo mkdir my_repo
          ```

    1. Copy your RPM files to the new directory:

          ```console
          sudo cp -rp ~/centos-stream-9/redhat/rpm/RPMS/* my_repo
          ```

    1. Initialize the directory as an RPM package repository:

          ```console
          sudo createrepo my_repo
          ```

1. To build an OS image in `.qcow` format to run in a QEMU virtual environment, run the `automotive-image-builder` script, substituting your values
     for the RPM repo directory location and the kernel version name:

     ```console
     ./automotive-image-builder build --distro cs9 --mode package --target qemu \
     --export qcow2 ~/sample-images/images/developer.mpp.yml my-custom-kernel.qcow2 \
     --define 'extra_repos=[{"id":"kernel","baseurl":"file:///<absolute-path-to-my_repo-directory>"}]' \
     --define 'kernel_version="5.14.0-515.464_mykernel.el9iv"'
     ```

    !!! note
        - The value for `kernel_version` should match the name of your custom kernel. You can find the name in the `~/centos-stream-9/redhat/rpm/RPMS/aarch64/`
        directory, which contains your custom built RPM files.

        - The value for `baseurl` can be any valid URL to an RPM repository that contains your custom kernel RPM files.

1. Run the image:

     ```console
     ./automotive-image-runner my-custom-kernel.qcow2
     ```

1. After the image boots in QEMU, log in as `root` using the password `password`.

1. Verify that your packaged software is present in your image by running the following command and verifying that the custom kernel name is returned:

     ```console
     uname -r
     ```

!!! Important
    If you build multiple images, run `dnf clean all` between builds to clear the build artifacts and reset the DNF cache.

## Build an AutoSD image with a custom kernel and run it on physical hardware

You can create an OS image in a specific format to install on specific physical hardware.

**Prerequisites**

- You have recursively cloned the [sample-images](https://gitlab.com/CentOS/automotive/sample-images) repository.

- You are running an AArch64 system with Internet access.

- You have successfully built RPM package files that contain kernel customizations.

**Procedure**

1. If you have not done so already, install [Podman](https://podman.io/docs/installation#centos-stream):

     ```console
     sudo dnf install podman
     ```

1. Go to the `automotive-image-builder` directory:

     ```console
     cd ~/sample-images/automotive-image-builder
     ```

1. Deploy the custom kernel RPM files:

    1. If you have not done so already, install the createrepo utility:

          ```console
          sudo dnf install createrepo
          ```

    1. Create a new directory to use as a local RPM repository:

          ```console
          sudo mkdir my_repo
          ```

    1. Copy your RPM files to the new directory:

          ```console
          sudo cp -rp ~/centos-stream-9/redhat/rpm/RPMS/* my_repo
          ```

    1. Initialize the directory as an RPM package repository:

          ```console
          sudo createrepo my_repo
          ```

1. Identify the image format appropriate for installation on your board:

     ```console
     ./automotive-image-builder list-exports
     ```

1. Identify the appropriate hardware target for your board:

     ```console
     ./automotive-image-builder list-targets
     ```

1. Build the image, specifying the appropriate target and the export file format and substituting your values
     for the RPM repo directory location and the kernel version name:

     ```console
     ./automotive-image-builder --container build --distro autosd --mode package \
     --target <target> --export <export_image_format> ~/sample-images/images/developer.mpp.yml my-custom-kernel.img \
     --define 'extra_repos=[{"id":"kernel","baseurl":"file:///<absolute-path-to-my_repo-directory>"}]' \
     --define 'kernel_version="5.14.0-515.464_mykernel.el9iv"'
     ```

    !!! note
        - The value for `kernel_version` should match the name of your custom kernel. You can find the name in the `~/centos-stream-9/redhat/rpm/RPMS/aarch64/`
        directory, which contains your custom built RPM files.

        - The value for `baseurl` can be any valid URL to an RPM repository that contains your custom kernel RPM files.

1. Follow the relevant procedure in the "Provisioning hardware" section to flash the OS image to your board.

1. After the image boots, log in as `root` using the password `password`.

1. Verify that your packaged software is present in your image by running the following command and verifying that the custom kernel name is returned:

     ```console
     uname -r
     ```

!!! note
    If you build multiple images, run `dnf clean all` between builds to clear the build artifacts and reset the DNF cache.

**Additional resources**

- See [Building an AutoSD image](building_an_os_image.md/#building-an-autosd-image) for more information about options available for building OS images.

- [Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image)

- [Sample manifest files](https://gitlab.com/CentOS/automotive/sample-images)
