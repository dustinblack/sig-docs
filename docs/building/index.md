# Building images

The Automotive SIG uses [OSBuild](https://www.osbuild.org/) as the tool to build
its images on CentOS Stream, Fedora, and RHEL hosts, with the option to build immutable images using
[OSTree](https://ostreedev.github.io/ostree/introduction/).

Before you build images, be aware of the following stipulations:

- **No cross-compilation**: To build AArch64 or x86_64 images, you must run OSBuild on the respective systems.
  Some options for AArch64 hosting include Raspberry Pi 4 or QEMU on Linux or macOS.
  For more information, see [AutoSD system running on a Raspberry Pi 4](autosd_pi4.md) or [AutoSD system running on qemu](autosd_qemu.md).

- **A subscribed RHEL system**: To build RHEL images, you must have access to a subscribed RHEL system to access to the entitlements.
  RHEL images can only be built on subscribed RHEL systems.

## Building on aarch64 macOS using Podman 5

On macOS, Podman uses Apple's virtualization framework and can't use a volume shared from host to build directly.
You have to build from a writable filesystem, for example, /root. The `auto-image-builder.sh` wrapper does that seamlessly by using the
Podman machine filesystem as a temporary working directory.

**Procedure**

1. Install `podman` from homebrew:

    ```console
    brew install podman
    ```

1. Initialize `podman machine` with rootful support for OSbuild:

    ```console
    podman machine init --rootful --disk-size 100
    podman machine start
    ```

1. Build the image:

    ```console
    sudo ./auto-image-builder.sh
    ```

!!! important

    The working directory (a checkout of sample-images, presumably) is copied to the Podman machine's root filesystem, because some commands cannot
    work from the containers `overlayfs` filesystem. After the build is completed the resulting image is copied back to the working directory.
    Take into account that the temporary copy inside the podman machine is always removed on each invocation, so there is no caching. And the whole
    content of the current working directory is copied each time.

## Finding a manifest

All manifests exist in the [Automotive SIG sample-images repository](https://gitlab.com/CentOS/automotive/sample-images).

**Procedure**

1. Clone the repository to download all manifests:

    ```console
    git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
    ```

1. Choose an image manifest file from the `images` folder.

## Running the image

You can either boot the image in QEMU/KVM or flash the image onto an SD card.

### Booting the image in QEMU/KVM

**Procedure**

- Boot the QCOW2 image in `virt-manager` or run it directly through QEMU using `runvm`:

```console
./runvm cs9-qemu-minimal-regular.x86_64.qcow2
```

!!! note

    On MacOS, you must install `qemu` to use `runvm`:

    ```
    brew install qemu
    ```
