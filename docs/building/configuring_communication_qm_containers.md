# Configuring communication between QM containers

The method to configure communication between applications in the QM partition is similar to
[Configuring communication between ASIL containers](configuring_communication_asil_containers.md#configuring-communication-between-asil-containers).
The difference between the procedures is the placement of the stages. Configuration stages related to ASIL applications belong in the `rootfs`
pipeline of the manifest file, whereas configuration stages related to QM applications belong in the `qm_rootfs` pipeline.

**Prerequisites**

* A custom manifest file, such as the manifest file that you created in
 [Embedding containerized applications in the QM partition](deploying_apps_in_qm_partition.md#embedding-containerized-applications-in-the-qm-partition)
* An existing QM container, such as the one you created in
[Embedding containerized applications in the QM partition](deploying_apps_in_qm_partition.md#embedding-containerized-applications-in-the-qm-partition)

**Procedure**

1. Configure the manifest to create a new container in the `qm_rootfs` pipeline of your manifest file. The example manifest already includes a local
container in the `qm_rootfs` pipeline called `auto-apps`, which acts as server in the communication channel. In this example, you pull the
`my-client-app` container from a registry, instead of from your local environment.

    ```console
      - type: org.osbuild.skopeo
          inputs:
            images:
              type: org.osbuild.containers
              origin: org.osbuild.source
              mpp-resolve-images:
                images:
                - name: localhost/my-client-app:latest
                  source: <your-container-registry>/my-client-app
                  tag: latest
    ```

1. To configure IPC between the server and client containers, create two Quadlet configuration files that mount the volumes that contain the UNIX
domain sockets in `/var/run/<your-dir>`. In the example code, `<your-dir>` is `ipc-demo`, but you can name the directory according to your own
conventions.

    1. Create a Quadlet configuration file `auto-apps.container`.
    2. Configure the `auto-apps` container that is acting as a server:

        ```console
        [Unit]
        Description=auto-apps container

        [Container]
        ContainerName=auto-apps
        Image=localhost/auto-apps:latest
        Network=none
        Volume=/run/ipc:/run/ipc
        SecurityLabelType=qm_container_ipc_t

        [Install]
        WantedBy=multi-user.target

        [Service]
        Restart=always
        ```

    3. Create a Quadlet configuration file `my-client-app.container`.
    4. Configure the `my-client-app` container that is acting as a client.

        ```console
        [Unit]
        Description=client container

        [Socket]
        ContainerName=my-client-app
        Image=localhost/my-client-app:latest
        Volume=/run/ipc:/run/ipc
        SecurityLabelType=qm_container_ipc_t

        [Install]
        WantedBy=multi-user.target

        [Service]
        Restart=always
        ```

1. Optional: Use `systemd` to create a UNIX socket file with the same name as the service with which the socket is associated, such as the
`auto-apps` service.
      1. Create the `systemd.socket` file `auto-apps.socket`:

          ```console
          [Unit]
          Description=An example systemd unix socket

          [Socket]
          ListenStream=%t/ipc/qm/ipc.socket
          RuntimeDirectory=ipc/qm
          SELinuxContextFromNet=yes

          [Install]
          WantedBy=sockets.target
          ```

      2. Add additional lines to the `auto-apps.container` file:

          ```console
          [Unit]
          After=auto-apps.socket
          Requires=auto-apps.socket
          ```

      3. Copy the `auto-apps.socket` file to the `/etc/systemd/system/` directory. Create a new `org.osbuild.copy` stage in the `qm_rootfs` pipeline
      of your manifest file:

          ```console
          - type: org.osbuild.copy
            inputs:
              qm_extra_files_0:
                type: org.osbuild.files
                origin: org.osbuild.source
                mpp-embed:
                  id: qm_ipc_socket
                  path: ../auto-apps.socket

            options:
              paths:
              - from:
                  mpp-format-string: input://qm_extra_files_0/{embedded['qm_ipc_socket']}
                to: tree:///etc/systemd/system/auto-apps.socket
          ```

      4. Enable the service to ensure that the socket is created at boot. Create a new `org.osbuild.systemd` stage in the `qm_rootfs` pipeline of
      your manifest file:

          ```console
          - type: org.osbuild.systemd
            options:
              enabled_services:
              - auto-apps.socket
          ```

      5. In the `org.osbuild.copy` stage of the `qm_rootfs` pipeline, add both Quadlet configuration files to `/etc/containers/systemd/`:

          ```console
          - type: org.osbuild.copy
            inputs:
              qm_extra_content_1:
                type: org.osbuild.files
                origin: org.osbuild.source
                mpp-embed:
                  id: qm_auto_apps_container
                  path: ../auto-apps.container
              qm_extra_content_2:
                type: org.osbuild.files
                origin: org.osbuild.source
                mpp-embed:
                  id: my_client_app_container
                  path: ../my-client-app.container

              options:
                paths:
                - from:
                    mpp-format-string: input://qm_extra_content_1/{embedded['qm_auto_apps_container']}
                  to: tree:///etc/containers/systemd/auto-apps.container
                - from:
                    mpp-format-string: input://qm_extra_content_2/{embedded['my_client_app_container']}
                  to: tree:///etc/containers/systemd/my-client-app.container
          ```

        !!! note
            You can include multiple inputs in each `org.osbuild.copy` stage.

1. Optional: Create a new `org.osbuild.mkdir` stage in the `qm_rootfs` pipeline to confirm that the destination directory exists:

    ```console
    - type: org.osbuild.mkdir
      options:
        paths:
          - path: /etc/containers/systemd
            exist_ok: true
    ```

    !!! note
        This step demonstrates how to avoid build failures. If the destination path does not exist, you must define a path, or the build process will
        fail. However, in the base AutoSD image, the `/etc/containers/systemd` directory already exists. For a preexisting directory, set the value
        of the `exist_ok` flag to `true` to prevent an `OSError` exception.

**Next steps**

* Now that you configured communication between QM containers, you can build your AutoSD image. For more information, see
[Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).
* Alternatively, you can continue customizing your image. For more information, see
[Containerizing applications](containerizing_apps.md#containerizing-applications).

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)
