# Using make manifests

Use `make manifests` to preprocess all existing manifests without building them.
You can use `make manifests` to verify that all combinations of options still work after a change or to inspect the resulting manifests.

In addition to the image types `*.img` and `*.qcow2`, the `makefile` also supports targets such as:

- `*.rootfs`
- `*.repo`
- `*.tar`
- `*.container`
- `*.ext4`

These targets are useful during development and testing. For more information, run `make help`.
