# Building an AutoSD image

The `automotive-image-builder` tool is a Python script that extends the functionality of the OSBuild tool to simplify how to create and customize
operating system (OS) images.

When you run the `automotive-image-builder` tool, it leverages OSBuild to build images based on `YAML` files called manifests. Manifests contain
specifications for your OS image. Manifests specify build details, such as which repositories and RPM packages to include, which services to enable,
and hostname and time zone configurations. Manifest files also describe stages that modify the file system of the image.

In this workflow, you have configured your own custom manifest file that you will use to build your own custom AutoSD image. For further reference,
you can also review the sample manifests that you can use to build images in the
[Automotive SIG repository](https://gitlab.com/CentOS/automotive/sample-images).

## Building and running your customized OS image

The `automotive-image-builder` repository includes a script that simplifies the build process. The `automotive-image-builder` script invokes OSBuild
to build the images by using manifest files that you specify.

**Prerequisites**

* A clone of the [automotive-image-builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder) repository
* Any customizations that you have made to the sample OSBuild manifest files, such as the manifest file that you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image)
and extended in the other sections in this guide
* OSBuild

**Procedure**

1. Build the AutoSD image. In this example, specify your manifest file and the `qcow2` image format in the `automotive-image-builder build` command:

     ```console
     ./automotive-image-builder build --distro cs9 --mode package --target qemu \
     --export qcow2 <my-manifest>.mpp.yml <my-image>.qcow2
     ```

    The example command demonstrates how to build a QCOW2 image for a virtual environment. For more information about building images for specific
    hardware, see
    [Provisioning hardware](../provisioning/flashing-texas-instruments.md).

    !!! note
        You can also include parameter overrides directly in the build command through the CLI. For more information, see
        [Overriding variables from the command line](building_an_os_image.md#overriding-variables-from-the-command-line)

    !!! note
        If you build multiple images, run `dnf clean all` between builds to clear the build artifacts and reset the DNF cache.

1. Run the image:

     ```console
     ./automotive-image-runner <my-image>.qcow2
     ```

1. After the image boots, log in as `root` using the password `password`.
1. Verify that your packaged software is present in your image:

     ```console
     rpm -q auto-apps dlt-daemon vsomeip3-routingmanager
     ```

Your image is now ready.

**Next steps**

* Now that you have built your AutoSD image, you can flash it onto an SD card. For more information, see
  [Flashing the image on an SD card](building_an_os_image.md#flashing-the-image-on-an-sd-card).

## Overriding variables from the command line

Use the `--define` variable to override variables in your manifest from the command line. The syntax for the `--define` variable is `--define var=value`.

A distro file normally defines these variables:

* `distro_name`: The name of the distro; default is `cs9`.
* `distro_version`: The version of the distro.
* `distro_baseurl`: The base URL of the distribution's repo; can be overridden to use a local mirror.
* `distro_module_id`: The is used for `dnf` modules; default `platform:el9`.
* `distro_repos`: The set of default repos for the distro.
* `distro_devel_repos`: The set of development repos for the distro.
* `distro_debug_repos`: The set of debug-info repos for the distro.

Other variables you might want to override include:

* `extra_repos`: A list of extra `yum` repos to install RPMs from; defaults to empty.
* `extra_rpms`: List of additional RPMs installed in most images; defaults to empty.
* `kernel_rpm`: The name of the kernel package used; defaults to `kernel-automotive`.
* `linux_firmware_rpm`: The name of the `linux-firmware` package used; defaults to `linux-firmware-automotive`.

* `osname`: The OS name (used in OSTree deployment); defaults to `centos`.
* `os_version`: The OS version (used in OSTree deployment); defaults to `$distro_version`
* `ostree_ref`: The name used for the ref when you commit to an `ostree` repo.
* `ostree_remote_name`: The name of the main OSTree remote; defaults to `auto-sig`.
* `ostree_repo_url`: The URL of the main OSTree remote; defaults to `http://10.0.2.100/`.

* `use_efi_runtime`: If true, enable efi runtime services; defaults to`true`.
* `kernel_opts`: Base kernel options.
* `kernel_loglevel`: Kernel log level; defaults to `4`.

* `root_password`: Password for root account; default `password`.
* `root_ssh_key`: SSH key for root account; default empty.
* `guest_password`: Password for `guest` account; default `password`.

* `ssh_permit_root_login`: If true, enables SSH login for root account; defaults to `false`.
* `ssh_permit_password_auth`: If true, enables password authentication with SSH; defaults to `false`.

* `display_server`: The name of the display server (`wayland` or `xorg`); defaults to `wayland`.

* `image_size`: The total size of the disk image in bytes as a string; defaults to 8 GB.
* `efipart_size`: The size of the EFI partition.
* `bootpart_size`: The size of the boot partition.
* `partition_label`: Either `gpt` (default) for gpt partition layout, or `dos` for dos partition layout (for rpi4).
* `static_uuids`: Set to `true` to use static default UUIDs for filesystems rather than generated. Default is `true`.
   If enabled, uses `rootfs_uuid`, `bootfs_uuid`, `parttab_uuid`, `efipart_uuid`,  `bootpart_uuid`, `rootpart_uuid`, and `luks_uuid` for specific
   values.

* `use_luks`: If true, encrypt the rootfs with LUKS; default `false`.
* `luks_passphrase`: The passphrase used to encrypt the rootfs. Default `password`.
* `luks_auto_unlock`: If true, automatically unlock the encrypted rootfs on boot with a key file.
   This is meant to be replaced later with a per-machine key. Default `false`.
* `luks_use_integrity`: If true, combine dm-integrity with encryption. This variable is experimental due to low performance. Default `false`.

* `dracut_add_modules`: Dracut modules to add.
* `dracut_omit_modules`: Dracut modules to omit.
* `dracut_filesystem`: Dracut filesystems to install (defaults to `vfat` and `ext4`).
* `dracut_add_drivers`: Special drivers to install into dracut.
* `dracut_install`: Special files to copy into dracut.

**Example**

1. Use `--define` to add some extra RPMs and to decrease `image_size` to 3 GB:

    ```console
    ./automotive-image-builder build --distro cs9 --mode package --target qemu \
    --export qcow2 <my-manifest>.mpp.yml <my-image>.qcow2 --define 'extra_rpms=["gdb","strace"]' --define 'image_size=3489660928'
    ```

1. Verify that you successfully defined the `image_size`, which is `virtual size` in the command output:

    ```console
    $ qemu-img info <my-image>.qcow2
    image: <my-image>.qcow2
    file format: qcow2
    virtual size: 3.25 GiB (3489660928 bytes)
    disk size: 251 MiB
    cluster_size: 65536
    Format specific information:
        compat: 1.1
        compression type: zlib
        lazy refcounts: false
        refcount bits: 16
        corrupt: false
        extended l2: false
    Child node '/file':
        filename: <my-image>.qcow2
        protocol type: file
        file length: 252 MiB (264110080 bytes)
        disk size: 251 MiB
    ```

1. Run the image:

     ```console
     ./automotive-image-runner <my-image>.qcow2
     ```

1. After the image boots, log in as `root` using the password `password`.

1. Verify that the additional RPMs are present in your image:

    ```console
    # rpm -q gdb strace
    gdb-14.2-3.el9.x86_64
    strace-5.18-2.el9.x86_64
    ```

**Additional resources**

* [Sample custom OSBuild manifest](ref_sample-yaml.md#sample-custom-osbuild-manifest)

## Flashing the image on an SD card

To install the OS on a hardware device, flash the image onto an SD card.

**Prerequisites**

* A built AutoSD image such as the one you created in
  [Building and running your customized OS image](building_an_os_image.md#building-and-running-your-customized-os-image)

**Procedure**

* Flash the image onto an SD card:

```console
dd if=<my-image>.qcow2 of=<SD card device path> status=progress bs=4M
```

!!! important

    You must change the value for `of=<SD card device path>` to match the block device path used in your system, for example `/dev/sdb`. If
    required, add `-bios /usr/share/OVMF/OVMF_CODE.fd` to QEMU command.
