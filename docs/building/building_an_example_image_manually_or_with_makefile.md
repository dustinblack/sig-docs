# Building an example image manually or with makefile

You can build an image manually or by using makefile.

## Building the image manually

Building an image manually is a two-step process.

**Procedure**

1. Pass the selected YAML manifest and some options to the `osbuild-mpp` tool:

    ```console
    osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D arch=\"aarch64\"
    ```

    This example command applies the options and resolves the package names against the repositories
    used to produce a JSON manifest with fully resolved versions of all packages.
    This JSON file is fully self contained and produces reproducible builds.

    The manifest has multiple options that can affect how the manifest is preprocessed, for example:

    - `image_type` specifies whether the system is _ostree_ or _regular (dnf)_ based.
    - `extra_rpms` passes a list object of extra RPMs to the built image.

    These options are passed to `osbuild-mpp` as separate `-D` flags.
    Each of these options are individually processed as JSON input, and therefore quotes must be escaped or nested correctly at the command line,
    for example:

    ```console
    osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D image_type=\"regular\" -D extra_rpms=[\"dnf\",\"vim-enhanced\"]
    ```

    or

    ```console
    osbuild-mpp images/minimal.mpp.yml /tmp/output.json -I . -D 'image_type="regular"' -D 'extra_rpms=["dnf","vim-enhanced"]'
    ```

1. After preprocessing, pass the resolved manifest to `osbuild`, which builds the image in a series of steps:

    ```console
    sudo osbuild /tmp/output.json --store store/ --output-directory out/ --export qcow2
    ```

    When you run `osbuild` you can choose which steps to export, typically either _image_ or _qcow2_:

    - Image is a raw image that can be written to disk.
    - QCOW2 is a format used by QEMU for image files.

## Using makefile to build the image

Simplify the build process by using makefile.

!!! note

    Makefile uses sudo, so you might be asked for your password during the build process.

**Procedure**

1. From the checkout directory, run `make`, using the correct image name as the target:

    ```console
    make cs9-qemu-minimal-regular.x86_64.qcow2
    ```

    This command preprocesses and builds the manifest for the current architecture and defines the image type to be `ostree`.
    This results in a file named `cs9-qemu-minimal-regular.x86_64.qcow2`, which is stored in the current directory.

    !!! note

        You can use `make` to build RAW images as well. For a full list of images types available for the current architecture, run `make help`.

1. Optional: Remove the `_build` directory to regain disk space:

    ```console
    rm -r _build
    ```

    !!! note

        During the build process, artifacts such as JSON files, RPMs, and cached parts from previous builds are stored in the `_build` directory.
        Everything in this directory is derived from other sources.
