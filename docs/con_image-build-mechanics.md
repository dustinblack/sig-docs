# Image build mechanics

The OS image that you deploy on the target hardware consists of AutoSD packages that you assemble into an OS image by using the AutoSD image build
tool, `automotive-image-builder`.

The Automotive SIG stores source files in public Git repositories, including all of the CentOS Stream or AutoSD source code, `SPEC`
files, and patches used to build AutoSD. The [RPM Package Manager](con_rpm-packages-and-the-rpm-package-manager.md) references this repository to
compile AutoSD RPM packages.

Red Hat engineering teams build those RPM packages into an OS image by using
[`automotive-image-builder`](https://gitlab.com/CentOS/automotive/src/automotive-image-builder), which composes a build configuration
specification, called a _manifest_. The compose operation takes a YAML-based automotive image manifest and a set of options that affect the compose
and resolves the manifest into an OSBuild JSON file. The JSON file is a precise build instruction for how to build an image with OSBuild with the
very specific software, such as selected packages and container images, listed in the compose.

There are several options you set to configure how `automotive-image-builder` builds the image:

`--distro`
: Define the package repositories for the distribution you intend to use. The default is "cs9".

    * Use `automotive-image-builder list-dist` to see the list of distributions.
    * To extend the `list-dist` with custom distributions, put them in a directory called `/some/dir/distro` and pass `--include /some/dir` to the
    argument list.

`--mode`
: Set to either "package" or "image". The default value is "image".

    * **Package** mode produces a readable and writeable OS that uses DNF to install a package, which is useful during development and testing.
    * **Image** mode produces an immutable OS image built with OSTree that supports atomic updates
    but not package-level modification, which is useful in production.

    !!! IMPORTANT
        In production, Red Hat only supports those Red Hat In-Vehicle OS images created by using `image` mode.

`--target`
: Define the target hardware. The default value is "QEMU". Use `automotive-image-builder list-targets` to see a list of the supported targets.

`--arch`
: Define the hardware architecture as either "x86_64" or "aarch64". Your native architecture is the default unless you specify a value.

`--export`
: Define the image file type that you want to build. You can view the available options with the `automotive-image-builder --list-exports` command.

For more information, see [Building an OS image](building/building_an_os_image.md) or the [Getting Started guide](getting_started.md) specific
to your host system Linux, MacOS,
or Windows.

**Additional resources**

* For more information about how to build automotive images, see
[automotive-image-builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder).
