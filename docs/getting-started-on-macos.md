# Getting started on MacOS

Set up your development environment with the prerequisite tools and repositories you need to quickly start building
AutoSD images in either a preconfigured container or a virtual machine (VM).

**Prerequisites**

* A machine running MacOS
* [Homebrew](https://brew.sh/)
* The local [clone you made](getting_started.md#cloning-autosd-sample-images-and-automotive-image-builder)
  of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images) with the
  `automotive-image-builder` submodule

## Quick start: Building AutoSD images

You can use the preconfigured AutoSD development container as a lightweight base development environment if you want to
build an AutoSD image without launching and configuring a development VM.

**Prerequisites**

* A local clone of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images)

**Procedure**

1. Install [Podman 5](https://podman.io/docs/installation#macos):

    ```console
    brew install podman
    ```

1. Initialize a Podman machine with rootful support for OSBuild:

    ```console
    podman machine init --rootful
    ```

1. Start the Podman machine:

    ```console
    podman machine start
    ```

1. Navigate to the `sample-images` directory:

    ```console
    cd sample-images
    ```

1. Build an image with the `auto-image-builder.sh` script:

    ```console
    sudo ./auto-image-builder.sh build --target qemu --export qcow2 images/developer.mpp.yml my-image.qcow2
    ```

    In this example, include the `images/developer.mpp.yml` OSBuild manifest file to build a developer image. The developer image
    has a number of utilities installed for development purposes.
    You can view the available sample manifests in `sample-images/images`, and you can create your own
    manifest that contains your custom configurations. For more information about the sample manifests,
    see [Sample OSBuild manifests](about-manifests.md). For more information about creating a custom manifest, see [Deploying software on
    AutoSD](building/deploying_sw_in_the_os_image.md).

    The `build` command takes a variety of inputs:

    * Use `--target` to set the target environment. The default is `qemu`.
    Use `qemu` to build an image that you can launch in a QEMU virtual machine. Run `--list-targets` to view the list of available options.
    * Use `--distro` to define the package repository that you want to use for the image build.
    The default is CentOS Stream 9 (`cs9`). Run `--list-dist` to view the list of available options.
    You can also extend this list with your own custom distribution.
    For more information, see [distributions](about-automotive-image-builder.md#distributions).
    * Use `--export` to set the export file format. Run `--list-exports` to view the list of available options.
    * Use `--mode` to set the type of OS image. This can be `package`, to build a package-based operating system image, or `image` to build an
    OSTree image. For development and testing purposes, use `package`. For production, use `image`.

    You can also include command-line overrides for certain parameters. For syntax and a full list of overrides,
    see [Overriding variables from the command line](building/building_an_os_image.md#overriding-variables-from-the-command-line).

## Quick start: Booting prebuilt AutoSD images in a QEMU VM

A virtualized AutoSD development environment is similar to a AutoSD production environment. The virtualized platform is useful for building and
testing applications intended to run on a AutoSD system or to build new AutoSD images that you can flash onto automotive hardware or use on other
systems.

Download and uncompress a prebuilt AutoSD operating system (OS) image, and then launch a VM from the image. This VM is your
development environment, where you can customize and build your own AutoSD images.

All prebuilt AutoSD images can be found in the [nightly repo](https://autosd.sig.centos.org/AutoSD-9/nightly/).
The repo features both x86_64 and aarch64 architecture images. To explore the sample images, see
[sample images](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/).
In this example, use the aarch64 `developer` image for Apple silicon. The `developer` image has extra storage compared to other images.

**Procedure**

1. Install [QEMU](https://www.qemu.org/download/#macos) and
  [XZ Utils](https://github.com/tukaani-project/xz):

    ```console
    brew install qemu xz
    ```

1. Identify the name of the latest nightly image for your host architecture, and store the value in a variable called `AUTOSD_IMAGE_NAME`.

    !!! note

        The Automotive SIG uploads images every day using unique build IDs, which causes the name of the image to change frequently.
        For more information about available nightly images, their purposes, and their naming conventions,
        see [Sample OS images](sample-non-sample-images.md).

      ```console
      $ export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/ | \
      grep -oE 'auto-osbuild-qemu-autosd9-developer-regular-aarch64-([0-9]+)\.([A-Za-z0-9]+)\.qcow2\.xz' | \
      head -n 1)"
      ```

1. Download the image:

    ```console
    curl -o autosd9-developer-regular-aarch64.qcow2.xz https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/$AUTOSD_IMAGE_NAME
    ```

1. Uncompress the compressed `.xz` image file:

    ```console
    xz -d autosd9-developer-regular-aarch64.qcow2.xz
    ```

1. Launch a VM from the image with the
  [`automotive-image-runner`](https://gitlab.com/CentOS/automotive/src/automotive-image-builder/-/blob/main/automotive-image-runner?ref_type=heads)
  script from the the `automotive-image-builder` directory of your local clone of the
[AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images):

    ```console
    cd sample-images/automotive-image-builder
    ./automotive-image-runner <path>/autosd9-developer-regular-aarch64.qcow2
    ```

1. Log in as the `root` user with the default password, `password`.

    !!! NOTE
        To enable ssh access, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`. Then you can access the machine
        with `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

## Quick start: Building customized AutoSD images in a QEMU VM

Repeat the procedure in
[Quick start: Booting prebuilt AutoSD images in a QEMU VM](#quick-start-booting-prebuilt-autosd-images-in-a-qemu-vm),
to download and run the latest nightly developer image, which has extra storage compared to the other sample images. Then, expand
the disk size, so you can use the `automotive-image-builder` tool to create customized system images using your custom manifest `.mpp.yml` file.

For more information about the preconfigured manifest files the Automotive SIG provides as starter examples you can modify, see the
[Sample OSBuild manifests](about-manifests.md#sample-osbuild-manifests).

For more in-depth information about how to package your applications and embed them in a customized manifest you can then use to generate
your customized OS image, see
[Packaging applications with RPM](building/packaging_apps_with_rpm.md) and
[Embedding RPM packages in the AutoSD image](building/packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image) sections.

**Prerequisites**

* The latest [nightly](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/)
  `autosd9-dev-reg-aarch64.qcow2` image

**Procedure**

1. Extend the virtual disk of your `.qcow2` development image, so that you have enough space to build
your custom AutoSD images and facilitate your development work.

      1. On the host, resize your development image. In this example, set the disk size to `30G`, which is 30GiB:

         ```console
         qemu-img resize autosd9-dev-reg-aarch64.qcow2 30G
         ```

      1. Launch your virtual AutoSD development environment:

         ```console
         cd sample-images/automotive-image-builder
         ./automotive-image-runner autosd9-dev-reg-aarch64.qcow2
         ```

      1. Log in with the `guest` user and the default password `password`.

        !!! NOTE
            To enable ssh access, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`. Then you can access the machine
            with `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

1. Change to the `root` user. The `root` user password is also `password`:

    ```console
    su -
    ```

1. Install the `parted` partition management tool to extend the file system:

    ```console
    dnf -y install parted
    ```

    1. Run `parted` to extend the size of `/dev/vda`:

        ```console
        parted /dev/vda
        ```

    1. Resize the `/dev/vda3` partition to fill the space available to that partition:

        ```console
        (parted) resizepart 3 100%
        ```

    1. Exit the `parted` tool:

        ```console
        (parted) quit
        ```

    1. Enlarge the file system:

        ```console
        resize2fs /dev/vda3
        ```

1. [Clone the AutoSD sample-images repository](getting_started.md#cloning-autosd-sample-images-and-automotive-image-builder)
   in your development VM.

      ```console
      git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
      ```

1. In your development VM, create a custom OSBuild manifest file that you can configure according to your requirements:

      ```console
      vim my-manifest.mpp.yml
      ```

    !!! note
        The Automotive SIG provides several sample manifest files in `sample-images/images/` you can reference. For more information about
        available sample images, see [Sample OS images](sample-non-sample-images.md). To view an example customized manifest, see
         [Sample customized OSBuild manifest](building/ref_sample-yaml.md). For more details about how to build and customize images, see
         [Deploying software on AutoSD](building/deploying_sw_in_the_os_image.md) and
         [Embedding RPM packages in the AutoSD image](building/packaging_apps_with_rpm.md#embedding-rpm-packages-in-the-autosd-image).

1. Build the OS image from your custom `my-manifest.mpp.yml`. In this example, build a `qcow2` format for the `qemu` target so that you can
launch your image in a VM:

    ```console
    cd sample-images
    sudo ./auto-image-builder.sh build --target qemu --export qcow2 my-manifest.mpp.yml my-image.qcow2
    ```
  <!-- Alex: build that uses the script fails when run inside a VM on macos. "OSError: Failed to open log file: /var/log/hawkey.log" -->

    !!! NOTE
        For more information about the export file types `automotive-image-builder` supports, see [Export formats](about-automotive-image-builder.md#export-formats) in the
        [`automotive-image-builder` options](about-automotive-image-builder.md#automotive-image-builder-options) section.

        For more information about image naming conventions, see [AutoSD sample images](sample-non-sample-images.md#autosd-sample-images).

1. Exit the VM and export the image file to the host:

    ```console
    scp -P 2222 "UserKnownHostsFile=/dev/null" guest@localhost:/home/guest/sample-images/my-image.qcow2 .
    ```

1. On the host, launch a VM from your new image:

    ```console
    ./automotive-image-runner my-image.qcow2
    ```

For more information about how to build your own customized AutoSD images, see [Building and running your customized OS image](building/building_an_os_image.md#building-and-running-your-customized-os-image).
