# RPM packages and the RPM Package Manager

RPM Package Manager (abbreviated with the recursive acronym, RPM) is a set of software tools used to build, install, uninstall, and otherwise manage
RPM packages, which are architcture-specific collections of compiled binary, configuration, and documentation files with the extension `.rpm`.

## RPM Package Manager history and advantages

Originally called the Red Hat Package Manager, Red Hat developed this tool to solve general problems with installing, updating, and auditing
software on UNIX. Two different UNIX archive formats dominated Linux OS distributions in the mid-1990s, achiver (`ar`) or copy in and out (`cpio`).
Some distributors, such as Debian, chose `ar` format for their `.deb` packages, while others, such as Red Hat, chose `cpio` format for their `.rpm`
packages. Both Linux distributor types built new package management systems based on their archive formats to solve problems they had with the UNIX
systems. These systems have the following features:

* Define the logic that determines the necessary order to install, update, or remove files
* Identify who had built the software using cryptographic signatures
* Check the integrity of downloaded software using cryptographic hashes
* Query what installed software its storage location on the system
* Verify software updates

Packaging software by using RPM Package Manager has several advantages:

**Standardized format**
:   Binary or source code, metadata, and scripts that automate installation, configuration, and maintenance

**Dependency management**
: Automatically resolves and installs necessary libraries and components

**Version control**
: Automates upgrade, rollback, or removal tasks for each package by its version number

**System consistency**
: Standardizes installation across many systems

**File management**
: The structured format for organizing and managing files enables better software identification, verification, rollback, and removal

**Security and integrity**
: Digital signatures confirm authenticity, preventing unauthorized modification and establishing trust in the software supply chain

**Distribution**
: Efficient sharing and deployment on a variety of Linux platforms

**Package verification**
: Tools that check for digital signatures, permissions, and other inconsistencies or issues

## RPM package types

There are two types of RPM package:

**Binary (RPM packages)**
: An architecture-specific, binary collection with the extension `.rpm` that has the compiled binary, configuration, and documentation files built
from source code and patches. RPM packages have the following advantages over distributing software in conventional archive files:

    * RPM manages software in the form of packages that you can install, update, or remove independently of each other, which makes the maintenance of
      an OS easier.
    * RPM simplifies the distribution of software because RPM packages are standalone binary files, similar to compressed archives. RPM builds
      RPM packages for a specific OS and hardware architecture. RPM packages contain files such as compiled executables and libraries that get
      installed into the appropriate paths on the filesystem.

**Source RPM packages (SRPM packages)**
: An architecture-agnostic source package with the extension `.src.rpm` that has the source code, a `SPEC` file that specifies how to compile the
source code, and any applicable patches to build and rebuild the binary RPM packages. Use SRPMs to take advantage of the following benefits:

    * Preserve the source code, `SPEC` file, and all packages for a specific RPM file name, version, and release, which is useful for tracking and
      debugging purposes.
    * Build a binary RPM on a different hardware platform or architecture.

Now that you understand the distinction between source and binary distributions, see the _Building and customizing OS images_ guide to build your
own.

## RPM files

An RPM file is a modified `cpio` archive that has various metadata that the `rpm` command interprets. The package uses the name, version, release,
architecture (NVRA) naming convention to help you select the version. For example:

_&lt;name>_-**_&lt;version>_**-_&lt;release>_.**_&lt;architecture>_**.rpm

kernel-**5.14.0**-427.20.1.382.el9iv.**aarch64**.rpm

Package maintainers use the release value as a mechanism to iterate on and provide different versions of a versioned upstream release. Therefore,
the same upstream release can have multiple binary RPM versions throughout the duration of its support lifecycle. This means that packagers can
provide patches or fixes to consumers directly through the RPM.

### Inside an RPM file

An RPM file has four sections:

**Lead**
: Identifying information for the file

**Signature**
: Checksums and GPG signatures to help show that the package is intact and signed by a trusted party

**Header**
: The name, version, release, copyright, and other data that you can query about installed software, available updates, changelogs, and other useful
data

**Payload**
: The files installed on the system, including scripts that might run before and after various triggers occur

### Basic RPM commands and utilities

As you begin to work with RPM-based systems, you need two basic commands:

* `rpm`, which you can use to query, install, upgrade, remove and verify the contents of packages
* `rpmbuild`, which you can use to build both RPM and SRPM packages

Because the `rpm` command does not resolve dependencies, the RPM Package Manager also uses the DNF package management utility, the successor to YUM.
The `rpm` command extracts the dependency list from the RPM file header. Then, DNF searches the metadata from the enabled repositories to find
whether a given software package or collection is available to install or upgrade and what dependencies the RPM file needs to run. DNF then checks
whether its dependencies are available:

* If the dependencies are available, DNF downloads and installs all necessary packages in an order that meets the listed dependencies.
* If the dependencies are not available, DNF lists the missing requirements so the system administrator can find them.

If you type `dnf update` without specifying a software package or collection, DNF checks all of your installed software to see what is newer in the
configured repositories and what dependencies you need to make those updates work. This does a similar set of downloads and upgrades to keep the
system working.

To use RPM Package Manager to build RPM image binaries, see
[Packaging applications with RPM](building/packaging_apps_with_rpm.md).

**Additional resources**

For more information about RPM package management for RHEL-driven software, see the following resources:

* [RPM Package Manager documentation at rpm.org](https://rpm.org/index.html)
* [RPM Package Manager from the Fedora System Administrator’s Guide](https://docs.fedoraproject.org/en-US/fedora/latest/system-administrators-guide/RPM/)
* [How to create a Linux RPM package](https://www.redhat.com/sysadmin/create-rpm-package)
* [RPM Packaging Guide](https://rpm-packaging-guide.github.io/)
