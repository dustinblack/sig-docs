# Introduction to OSTree

Libostree, formerly known as OSTree or ostree is the mechanism that enables AutoSD to be an image-based operating system (OS). Despite officially
being named Libostree, the project is best known as OSTree.

Similar to git or container images, OSTree relies on a content-addressed object store with branches (or refs) that track different states of an
image or different images for one or more systems. Similar to git, where you can check out a precise version of the repository given a reference
branch, commit hash, or tag, with OSTree you can check out a precise version of your OS. The OS that you check out is read-only, except for two
persistent writable directories that are preserved across checkout operations: `/etc` and `/var`, and a new top level `ostree` directory.

More specifically, there is an OSTree repository on each client machine in the `/ostree/repo` directory, and a set of deployments are stored in it
at `/ostree/deploy/$STATEROOT/$CHECKSUM`. Each deployment is primarily composed of a set of hard links into the repository. This means each version
is deduplicated; an upgrade process only uses disk space proportional to the new files, plus some constant overhead. In AutoSD, this facilitates an
A/B update model that is filesystem-based rather than partition-based. This model saves disk space for the system, supporting the ability to have
more than two versions (A/B) of the system, all while preserving the atomicity of updates and the possibility of rollbacks.

**Security and reliability**

In an OSTree model, OS read-only content is kept in the classic Unix `/usr` directory. By default, OSTree comes with code to create a Linux
read-only bind mount to prevent inadvertent corruption. In AutoSD, this is combined with composefs to make the OS fully tamperproof from attacks,
as well as accidents, such as cosmic rays causing random bit flips. For more information about composefs, see &lt;xref>. Composefs also improves
performance in OSTree-based systems with faster reads and better deduplication. There is exactly one `/var` writable directory shared between each
deployment for a given OS. The OSTree core code does not touch content in this directory. The OS code determines how to manage and upgrade the
`/var` directory.

**Automated rollback**

Each deployment has its own writable copy of the configuration store `/etc`. During upgrades, OSTree performs a basic three-way diff and applies
any local changes to the new copy while leaving the old copy untouched. At boot time, OSTree checks the health of the system with the greenboot
tool. You can extend this health check to verify any aspect of the system upon boot. If greenboot fails a check, the update is marked as failed and
the system automatically reboots into the last known good version, thus allowing automated rollback. For more information about greenboot, see
[Automating RHEL for Edge image rollback with GreenBoot](https://www.redhat.com/en/blog/automating-rhel-edge-image-rollback-greenboot).

**Low-bandwidth updates**

As a content-addressed object store, on-disk deduplication is inherent to OSTree and downloads are very efficient. You can generate binary diffs
between two references in an OSTree repo, which means that you can have byte-level diffs between two versions of a single image or between two
separate images. This results in a bandwidth-efficient update mechanism.

By leveraging OSTree, AutoSD delivers an image-based, immutable operating system. Combined with composefs, AutoSD is tamper proof, and supports
atomic A/B updates and rollbacks. Together with a bandwidth-efficient update mechanism, you can dynamically install containerized applications and
manage the application lifecycle independently from the underlying OS lifecycle.

To learn more about OSTree, see the [OSTree project documentation](https://ostreedev.github.io/ostree/).
