# Hardware enablement program

The hardware enablement program fosters collaboration and enables the development of intelligent and connected vehicle applications.
Its primary objective is to enable reference hardware platforms for Red Hat In-Vehicle Operating System (OS) through the
[CentOS Stream Automotive Special Interest Group's (SIG's) Automotive Stream Distribution (AutoSD)](https://blog.centos.org/2022/03/centos-automotive-sig-announces-new-autosd-distro/).

The CentOS Stream Automotive SIG serves as Red Hat's open source community, where anyone can build, test, experiment, and benefit from
shared infrastructure to contribute to automotive software. AutoSD is the [upstream](https://www.redhat.com/en/blog/what-open-source-upstream)
binary distribution that serves as the public, in-development preview of Red Hat In-Vehicle OS, similar to how CentOS Stream operates
in relation to Red Hat Enterprise Linux (RHEL). AutoSD is based on CentOS Stream, with a few divergences, such as an
automotive-specific kernel rather than CentOS Stream's kernel package. Red Hat In-Vehicle OS is the RHEL-based, safety-certified,
software-defined vehicle (SDV) OS currently in development by Red Hat.

![Diagram shows code flow from open source projects, like Linux mainline, to Fedora, Fedora Enterprise Linux Next, or ELN, to CentOS Stream. CentOS Stream receives contributions from many CentOS special interest groups, or SIGs, which then flow to both RHEL and AutoSD. The Automotive SIG contributes AutoSD code back to various open source projects. Finally, RHEL and AutoSD code converges to form Red Hat In-Vehicle OS.](img/content-flow-diagram.png "AutoSD in the Red Hat In-Vehicle OS upstream and downstream code flow")

As a CentOS Stream Automotive SIG community member, you can use AutoSD to innovate independently in the open, or you can
partner with Red Hat to enable your hardware on the safety-certified, production-ready in-vehicle OS.
The hardware enablement program empowers you with an efficient process to integrate your hardware platform
into the Red Hat ecosystem through AutoSD and
[certify](https://connect.redhat.com/en/partner-with-us/hardware-certification-overview) your safety-compliant hardware to run
Red Hat In-Vehicle OS.

!!! IMPORTANT
    Although it incorporates features that support functionally safe automotive Linux, **AutoSD is not ISO 26262 certified**.

    Out-of-context functional safety certification applies only to Red Hat In-Vehicle OS.
    Hardware vendors are responsible for the out-of-context certification of their hardware.
    Automakers are responsible for the in-context ISO 26262 certification of their implementation of the
    in-vehicle OS on their target hardware, with or without support from software and hardware vendors,
    depending on their contractual agreements.

To cater to different hardware partner prototyping and development needs, Red Hat has two distinct tracks.

* **Experimental**: The experimental track is for hardware suppliers to work mostly independently to enable hardware to work with
Linux mainline, Fedora Always Ready Kernel (ARK), or CentOS AutoSD `kernel-automotive`. If you are a silicon vendor who prefers
to test the compatibility of your hardware with AutoSD independently, the experimental track is ideal for initial testing that can give you
valuable insights and make adjustments.

* **Production**: The production track offers a comprehensive solution for hardware suppliers who are ready to certify and release your hardware
platforms to the automotive market with support for Red Hat In-Vehicle OS. This track encompasses enablement, integration, and a rigorous
hardware certification process that ensures your reference platform meets the stringent stability, reliability, and security requirements
that the automotive industry expects from you, as an automotive hardware vendor, and Red Hat. After certification, your hardware platform joins
the official list of supported devices available to Red Hat automotive customers.

![Is your hardware experimental or intended for production? If your hardware is intended for production, the preferred process is to submit your merge request to first to Linux mainline, then to Fedora Always Ready Kernel, or ARK, then to CentOS. Prepare one or more patches and submit them as merge requests. Get reviews from the Linux mainline, Fedora, or CentOS maintainers. Were the merge requests accepted? If yes, coordinate with the maintainers to integrate your patches into the Linux mainline, Fedora ARK, and CentOS trees, respectively. If patches were not accepted, make adjustments based on the feedback and resubmit. If your hardware is experimental but you prefer not to use the preferred process, you can skip Linux mainline development, and prepare a merge request to submit to Fedora ARK or CentOS maintainers instead. After your changes are accepted by CentOS maintainers into the AutoSD kernel, send a request to Red Hat for enablement. You confirm your board works with the Linux mainline, Fedora ARK, and CentOS AutoSD, and then provide your board to Red Hat. Red Hat verifies that your board works with Linux mainline, Fedora ARK, and CentOS AutoSD, and then Red Hat builds a new automotive kernel and releases a new version of Red Hat In-Vehicle OS. You run the Red Hat automotive test suite and submit the results to Red Hat. Red Hat reviews the test results, updates the list of certified boards, and issues your hardware certification.](img/HWEnablementFlow.png "Red Hat In-Vehicle OS Experimental and Production tracks of the hardware enablement workflow")

As you develop your hardware, you can work on one or both tracks simultaneously. For instance, you might opt to begin on the experimental track to
validate a proof of concept while concurrently working to upstream your drivers and initiate progress on the production track. This parallel approach
allows you to explore different avenues and tailor your journey to best suit your unique requirements. Even within the experimental track, Red Hat
recommends that you upstream your drivers into the
[Linux mainline kernel, maintained by Linus Torvalds,](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/), so you
can benefit from the stabilizing work done there and its downstream repositories and test your drivers with AutoSD.

When you and Red Hat agree to move your work with AutoSD forward on the production track, Red Hat expects that your drivers are already accepted
upstream, so Red Hat can backport them in the next possible Red Hat In-Vehicle OS `kernel-automotive` release.
The Red Hat In-Vehicle OS development team must ensure `kernel-automotive` maintains upstream traceability to the
Linux mainline kernel to effectively validate automotive functional safety, support the long maintenance cycles necessary
for software in the automotive industry, and accelerate production hardware enablement.

Red Hat provides you with a hardware test suite to run during the production track that has procedures to certify your hardware to run
Red Hat software. The test suite is accompanied by a guide with information about the entire certification process, test methodology,
results evaluation, and instructions for how to set up the certification environment, test the systems or components you intend to certify,
and submit the results to Red Hat for verification. After Red Hat reviews and approves these test results, Red Hat lists your hardware in the
 [Red Hat Ecosystem Catalog](https://catalog.redhat.com/hardware).

## Ideal hardware

In the spirit of collaboration, these criteria are aspirations during the experimental track. If you choose to advance your
AutoSD development towards the production track, these items will be treated more definitively as requirements.
Red Hat considers exceptions on a case-by-case basis.

1. Hardware suppliers must meet driver standards set by Red Hat for the AutoSD stack to ensure seamless integration and
   compatibility within our software ecosystem.
1. To reduce complexity, enhance interoperability, and facilitate efficient integration, hardware suppliers must use current Red Hat
   build environment technology, such as
   [Red Hat Package Manager (RPM)](https://rpm.org/) and [OSTree](https://ostreedev.github.io/ostree/).
1. To enable the advanced virtualization capabilities necessary for intelligent and connected vehicle applications, the hardware platform
   must support EL2 privileges for the aarch64 architecture or "ring-1" for the x86 architecture to integrate with
   [KVM](https://linux-kvm.org/page/Main_Page), so you can launch virtual machines.
1. To contribute to a richer user experience and open doors to a broader range of automotive use cases, your hardware must include an
   upstreamed GPU that provides optimal performance and support for graphics-intensive applications.
1. Hardware for aarch64 architecture must support one of the standard ARM
   [SystemReady](https://www.arm.com/architecture/system-architectures/systemready-certification-program) profiles.
   For example, compliance with profiles like SystemReady-IR or SystemReady-ES would enhance the system's reliability,
   robustness, and compatibility with industry standards.

## Out-of-tree driver modules

The CentOS Automotive SIG permits hardware drivers that you decide to maintain out-of-tree.
However, AutoSD does not include out-of-tree drivers in its distributions or sample images.
AutoSD does not include proprietary code or code that cannot be accepted upstream to enable out-of-tree drivers.
To work with AutoSD, your out-of-tree driver must be able to compile against an AutoSD binary build.
In other words, the the out-of-tree driver must work without kernel recompilation.

## Next steps

To enable your hardware on AutoSD and collaborate in the open source automotive ecosystem,
see the [Upstreaming drivers procedure](proc_upstreaming-drivers.md).

**Additional resources**

* [Kernel Virtual Machine (KVM) open source project](https://linux-kvm.org/page/Main_Page)
* [Virtual machine components and their interaction in RHEL](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/introducing-virtualization-in-rhel_configuring-and-managing-virtualization#rhel-virtual-machine-components-and-their-interaction_introducing-virtualization-in-rhel)
* [Driver Implementer's API Guide](https://www.kernel.org/doc/html/latest/driver-api/index.html)
* [Building External (out-of-tree) Modules](https://docs.kernel.org/kbuild/modules.html)
