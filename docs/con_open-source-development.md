# Open source development

Red Hat founded the [CentOS Automotive SIG](https://sigs.centos.org/automotive/) to curate [AutoSD](https://autosd.sig.centos.org/) as the open,
automotive-specific upstream for Red Hat In-Vehicle OS, where open source community members, partners, customers, and public enthusiasts work
together to prototype cloud-native SDV concepts that might someday become Red Hat In-Vehicle OS features.

Red Hat uses [an open source software development model](https://www.redhat.com/en/resources/open-source-software-life-cycle-brief) to conceive
enterprise open source products and solutions. The [open source way](https://www.redhat.com/en/about/open-source) for software creation is a set of
principles built upon an open forum for ideas where communities can solve problems and develop new technology.

![Conceptual diagram of how Red Hat takes software from communities to the enterprise: Red Hat sponsors or personnel participate in more than a million upstream communities such as Apache](img/communities-to-enterprise.png)

Red Hat communities-to-enterprise paths generate [certified solutions](https://access.redhat.com/articles/2918071) or platforms with guides that
assist customers with their own international standards compliance.

## Additional resources

* [Creating better technology with open source](https://www.redhat.com/en/about/open-source)
* [Understanding the open source software life cycle](https://www.redhat.com/en/resources/open-source-software-life-cycle-brief)
* [Red Hat Blog: What is an open source upstream?](https://www.redhat.com/en/blog/what-open-source-upstream)
