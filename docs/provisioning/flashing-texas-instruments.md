# Building and flashing images on Texas Instruments (TI)

Follow this procedure to flash an AutoSD image onto a Texas Instruments (TI) board.

**Prerequisites**

* An AArch64 system with internet access
* An SD card
* A TI board or platform supported by AutoSD

## Obtaining an image

Use the `automotive-image-builder` tool to build AutoSD images for these TI boards:

* SK-AM62x Sitara (`am62sk`)
* SK-AM69 Jacinto (am69sk`)
* TDA4 EVM (`tda4vm_sk`)
* J784S4 EVM (`j784s4evm`)

For these boards, you can download a prebuilt image if you don't want to build your own:

* SK-AM69 Jacinto (am69sk`)
* J784S4 EVM (`j784s4evm`)

## Building the AutoSD image

**Procedure**

1. Clone the `automotive-image-builder` repository:

     ```console
     git clone https://gitlab.com/CentOS/automotive/src/automotive-image-builder.git
     ```

1. Build the TI image for your TI board:

     ```console
     cd automotive-image-builder
     ./automotive-image-builder --container build --target <model-name> --mode package --export image <manifest> .
     ```

   Replace `<model name>` with the string corresponding to your board, such as `am62sk`.
   Replace `<manifest>` with your custom OSBuild manifest file, in the .mpp.yml format.
   If you do not have your own manifest file, you can build an AutoSD image with the `automotive-image-builder/example.mpp.yml` manifest file.

1. Continue with [Flashing the image onto an SD card](#flashing-the-image-onto-an-sd-card).

## Downloading a prebuilt image

**Procedure**

1. Identify the model name of your TI board as used in the image name.
See [the nightly build directory](https://autosd.sig.centos.org/AutoSD-9/nightly/TI/).
The model name is something like `am69sk` (for SK-AM69 Jacinto) or `j784s4evm` (for J784S4 EVM),
as seen after `auto-osbuild-` in the nightly build directory.

1. Store the value of the latest nightly image in a variable called `AUTOSD_IMAGE_NAME`.
This example is for the `am69sk` model. Replace that piece of the command if you are using a different board.

     ```console
     export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/TI/ | grep -oE 'auto-osbuild-am69sk-autosd9-qa-regular-aarch64-[0-9]+\.[A-Za-z0-9]+\.raw\.xz' | head -n 1)"
     ```

1. Download the image.

     ```console
     wget -O auto-osbuild-am69sk-autosd9-qa-regular.raw.xz http://autosd.sig.centos.org/AutoSD-9/nightly/TI/$AUTOSD_IMAGE_NAME
     ```

1. Uncompress the compressed `.xz` image file:

     ```console
     xz -d auto-osbuild-am69sk-autosd9-qa-regular.raw.xz
     ```

1. Continue with [Flashing the image onto an SD card](#flashing-the-image-onto-an-sd-card).

## Flashing the image onto an SD card

1. To flash the uncompressed image onto an SD card, replace *`</dev/sdX>`* with your block device.

     ```console
     dd if=auto-osbuild-am69sk-autosd9-qa-regular.raw of=</dev/sdX> status=progress bs=4M
     ```

1. Set the onboard DIP switches to SD boot mode `SW11[1-8] = 1000 0010 SW7[1-8] = 0000 0000`.

1. Plug in the SD card, and then start the system.

**Additional Resources**

* [TI AM69 SK Hardware Setup Guide](https://software-dl.ti.com/jacinto7/esd/processor-sdk-linux-am69/09_02_00_05/exports/docs/linux/How_to_Guides/Hardware_Setup_with_CCS/AM69_SK_Hardware_Setup.html)
* [TI J784S4 EVM Hardware Setup Guide](https://software-dl.ti.com/jacinto7/esd/processor-sdk-linux-j784s4/09_01_00_06/exports/docs/linux/How_to_Guides/Hardware_Setup_with_CCS/J784S4_EVM_Hardware_Setup.html)
* [TI AM62x SK EVM User's Guide](https://www.ti.com/lit/ug/spruj40c/spruj40c.pdf)
* [TI Jacinto TDA4 User's Guide](https://www.ti.com/document-viewer/lit/html/spruj62)
